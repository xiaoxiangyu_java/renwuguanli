//package home.study.xxy.bs.assignment;
//
//import home.study.xxy.bs.entity.ComJob;
//import org.springframework.stereotype.Component;
//
//import java.text.DateFormat;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.concurrent.DelayQueue;
//import java.util.concurrent.Delayed;
//import java.util.concurrent.TimeUnit;
//
///**
// * @author xiaoxiangyu
// * @create 2022-05-28 12:00
// * @功能：最短时间优先
// */
//@Component
//public class ShortTimeAssTest implements Assignment,Delayed {
//
////    @Scheduled(fixedDelay = 60 * 1000)
////    @Scheduled(cron = "0/7 * * * * ?")
//
//    private long delayTime;
//
//    private long expire;
//
//    /**
//     * 分配信息
//     */
//    private List<HashMap<String, List<String>>> jobsToWork;
//
//
//
//    public void setDelayTime(long delayTime) {
//        this.delayTime = delayTime;
//        this.expire = System.currentTimeMillis() + delayTime;
//    }
//
//    @Override
//    public List<HashMap<String, List<String>>> assJobs(ComJob jobId) {
//        return null;
//    }
//
//    @Override
//    public List<HashMap<String,List<String>>> getDelayQueue(long delayTime, ComJob jobId){
//        DelayQueue<ShortTimeAssTest> shortTimeAsses = new DelayQueue<>();
//        setDelayTime(delayTime);
//        shortTimeAsses.put(this);
//        try {
//            List<HashMap<String, List<String>>> hashMaps = shortTimeAsses.take().assJobs();
//            setJobsToWork(hashMaps);
//
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        return jobsToWork;
//    }
//
//    @Override
//    public List<HashMap<String, List<String>>> assJobs() {
//
//        System.out.println("ceshi22  assJobs" + DateFormat.getDateTimeInstance().format(new Date()));
//
//        return null;
//    }
//
//    public void setJobsToWork(List<HashMap<String, List<String>>> jobsToWork) {
//        this.jobsToWork = jobsToWork;
//    }
//
//    @Override
//    public long getDelay(TimeUnit unit) {
//        return unit.convert(this.expire -System.currentTimeMillis(),TimeUnit.MILLISECONDS);
//    }
//
//    @Override
//    public int compareTo(Delayed o) {
//        return (int)(getDelay(TimeUnit.MILLISECONDS) - o.getDelay(TimeUnit.MILLISECONDS));
//    }
//}

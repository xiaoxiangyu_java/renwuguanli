package home.study.xxy.bs.assignment;

import home.study.xxy.bs.entity.ComJob;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author xiaoxiangyu
 * @create 2022-05-28 11:51
 * @功能： 分配接口 spi
 *
 */
@Service
public interface Assignment {

    /**
     * 清楚所有未分配任务  查询所有未分配的
     * @return
     */
    void assJobs();

    /**
     * 在新建任务时 调用
     * @param jobId
     * @return
     */
    void assJobs(ComJob jobId);

    /**
     * 延时队列
     */
    void getDelayQueue(long delayTime, ComJob jobId);

}

package home.study.xxy.bs.assignment;

import home.study.xxy.bs.entity.ComJob;
import home.study.xxy.bs.entity.LoginUser;
import home.study.xxy.bs.mapper.JobMapper;
import home.study.xxy.bs.mapper.UserRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author xiaoxiangyu
 * @create 2022-05-28 12:00
 * @功能：最短时间优先
 */
@Component
public class ShortTimeAss implements Assignment,Delayed {

//    @Scheduled(fixedDelay = 60 * 1000)
//    @Scheduled(cron = "0/7 * * * * ?")

//    private

    /** TODO 给余留或者放弃的库存任务 分配最佳选项
     * @param job
     * @return
     */

    private long delayTime;

    private long expire;

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Autowired
    private JobMapper jobMapper;

    /**
     * 分配信息
     */
    private List<HashMap<String, List<String>>> jobsToWork;



    public void setDelayTime(long delayTime) {
        this.delayTime = delayTime;
        this.expire = System.currentTimeMillis() + delayTime;
    }

    @Override
    public void getDelayQueue(long delayTime,ComJob jobId){
        DelayQueue<ShortTimeAss> shortTimeAsses = new DelayQueue<>();
        setDelayTime(delayTime);
        shortTimeAsses.put(this);
        try {
            if (jobId == null){
                shortTimeAsses.take().assJobs();
            }else{
                shortTimeAsses.take().assJobs(jobId);
            }


        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     *  测试
     */
    @Override
    public void assJobs() {

        System.out.println("ceshi  assJobs" + DateFormat.getDateTimeInstance().format(new Date()));

    }


    /**
     * 添加项目后两个小时通过延时分配的任务的情况
     * 只需要针对单个项目，进行选择最佳接受对象
     * 循环每个工作人员，给每个人评分
     * @param job
     * @return
     */
    @Override
    public void assJobs(ComJob job) {

        //获取分配需要的接受任务信息  查询所有工作人员信息和等级信息
        List<Map> list = userRoleMapper.queryAllUserRole();


        /**
         * 开始评分 。。 采取扣分制 满分100 初始90 等级越高加分 高一级 加 5 分
         * 等级不够 ，扣100分
         */

        HashMap<String, Double> grader = new HashMap<>();
        if (list != null && list.size() > 0){
            Map map1 = null;
            int jobsLv = 1;
            for (int i = 0; i < list.size(); i++) {
                map1 = list.get(i);
                String user_id = map1.get("user_id")+"";
                if (grader.get(user_id) != null
                        && grader.get(user_id) < 0){
                    continue;
                }
                if (grader.get(user_id) == null){
                    jobsLv = 1;
                    grader.put(user_id,90.0);

                    //任务第一次进来 会判断等级打分
                    int lv = Integer.parseInt(map1.get("role").toString().split("lv_")[1]);
                    int jobLv = Integer.parseInt(job.getJobLv());
                    if (lv < jobLv ){
                        grader.put(user_id,grader.get(user_id) - 100.0);
                    }else if (lv > jobLv) {
                        //高出等级 加分
                        grader.put(user_id,(lv - jobLv) * 5 + grader.get(user_id));
                    }
                }


                /**
                 * 其他情况   规则
                 * 未完成任务多 减分
                 * 任务越多 递增加分 1.1倍
                 * 时间越近 按比例减分 时间比
                 *
                 */

                //在有任务权限在进行接下来判断   查询时使用左连接，如果没有jobtime就是没有接受任务
                if (grader.get(user_id) > 0 && map1.get("job_time") != null){
                    //任务数
                    jobsLv *= 1.1;
                    grader.put(user_id,grader.get(user_id) - jobsLv);

                    //时间
                    long jobTime = ((Date) map1.get("job_time")).getTime();
                    long createTime = ((Date) map1.get("create_time")).getTime();
                    long date = new Date().getTime();



                    double l = (BigDecimal.valueOf(date - createTime)).divide(BigDecimal.valueOf(jobTime - date),2, RoundingMode.CEILING).doubleValue();
                    double a = grader.get(user_id);
                    grader.put(user_id, a - l);

                }

            }
        }

        //找出最高分
        Set<Map.Entry<String, Double>> entries = grader.entrySet();
        double maxGrader = -999.0;
        String userId = null;
        for (Map.Entry<String, Double> entry : entries) {
            if (entry.getValue() > maxGrader){
                maxGrader = entry.getValue();
                userId = entry.getKey();
            }
        }

        //获取到最佳匹配 工作人员就分配

        job.setJobState("2");
        job.setStaffId(userId);
        // 开始分配
        jobMapper.assJob(job);





    }

    public void setJobsToWork(List<HashMap<String, List<String>>> jobsToWork) {
        this.jobsToWork = jobsToWork;
    }

    @Override
    public long getDelay(TimeUnit unit) {
        return unit.convert(this.expire -System.currentTimeMillis(),TimeUnit.MILLISECONDS);
    }

    @Override
    public int compareTo(Delayed o) {
        return (int)(getDelay(TimeUnit.MILLISECONDS) - o.getDelay(TimeUnit.MILLISECONDS));
    }
}

package home.study.xxy.bs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import home.study.xxy.bs.entity.Bill;
import home.study.xxy.bs.entity.ComJob;
import home.study.xxy.bs.entity.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface BillMapper  extends BaseMapper<Bill> {
    /*
     * 根据用户id查所有的订单
     * */
    List<Bill> queryAllBill(@Param("bill") Bill bill, @Param("pageVo") Map pageVo);

    /**
     * 根据用户id查询总条数
     * */
    int queryBillByIdCount(@Param("bill") Bill bill);

    /**
     * 添加任务时添加账单
     * */
    int addBill(@Param("job") ComJob comJob);
    /**
     * 完成任务时修改账单状态
     * */
    int changeBill(@Param("job") ComJob comJob);
}

package home.study.xxy.bs.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import home.study.xxy.bs.entity.Page;
import home.study.xxy.bs.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
public interface LoginMapper extends BaseMapper<User> {

    /**
     * 登录
     * */
    int loginCheck(@Param("username") String userName,@Param("pwd") String pwd);

    /**
     * 查询总条数
     * */
    int queryUserCount();


    /**
     * 查询所有
     * */
    List<User> queryAll(@Param("user") User user,@Param("pageVo") Map pageVo);

    /**
     * 注册新用户
     * */
    int addUser(User user);
    /**
     *
     * */
//    int countUserName(@Param("userI") User user);
    /**
     * 查询登录信息
     * */
    User getLoginData(@Param("username")String userName,@Param("pwd") String pwdMd5);

    /**
     * spring security 认证查询
     * @param userName
     * @param pwdMd5
     * @return
     */
    User getUserData(@Param("username")String userName);

    /**
     * 修改信息
     * */
    int changePwd(@Param("user") User user);

    /**
     * 修改权限信息
     * */
    int updateAdmin(@Param("user") User user);

    /**
     * 批量新增
     * @param userList
     * @return
     */
    int insertUsers(@Param("userList") List<User> userList);

}

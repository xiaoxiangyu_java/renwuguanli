package home.study.xxy.bs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import home.study.xxy.bs.entity.ManagementText;
import home.study.xxy.bs.entity.Role;
import org.apache.ibatis.annotations.Mapper;

/**
 * 类描述：
 *   Mapper 接口
 *
 * @author chenjs
 * @date 2022-05-17 15:57
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {

}

//package home.study.xxy.bs.mapper;
//
//import home.study.xxy.bs.entity.ComJob;
//import home.study.xxy.bs.entity.Order;
//import home.study.xxy.bs.entity.Page;
//import org.apache.ibatis.annotations.Mapper;
//import org.apache.ibatis.annotations.Param;
//
//import java.util.List;
//import java.util.Map;
//
///**
// * @title: OrderServiceMapper
// * @功能:
// * @Author Xiao Xiangyu
// * @Date: 2021/4/19 21:51
// * @Version 1.0
// */
//@Mapper
//public interface OrderMapper {
//
//    /*
//    * 根据id查所有的订单
//    * */
//    List<Order> queryAllOrder(@Param("order") Order order, @Param("pageVo") Map pageVo);
//
//    /**
//     * 根据用户id查询总条数
//     * */
//    int queryOrderByIdCount(@Param("order") Order order);
//}

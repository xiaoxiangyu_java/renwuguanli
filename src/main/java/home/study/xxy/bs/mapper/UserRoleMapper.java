package home.study.xxy.bs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import home.study.xxy.bs.entity.ManagementText;
import home.study.xxy.bs.entity.UserRole;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 类描述：
 *   Mapper 接口
 *
 * @author xiaoxiangyu
 * @date 2022-05-17 16:14
 */
@Mapper
public interface UserRoleMapper  extends BaseMapper<UserRole> {

    List<String> queryUserRole(String userId);

    List<Map> queryAllUserRole();


}

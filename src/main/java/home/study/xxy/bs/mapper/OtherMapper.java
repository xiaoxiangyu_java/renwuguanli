package home.study.xxy.bs.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import home.study.xxy.bs.entity.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
public interface OtherMapper  extends BaseMapper<ManagementText> {

    /**
     * 登录时统计流量
     * */

    void addCount(@Param("date") Date date);

    /**
     * 查询折线图流量
     */
    List<CountDay> countSum();

    /**
     * 查询公告
     */
    List<ManagementText> queryPQ(@Param("text") ManagementText managementText, @Param("pageVo") Map pageVo);

    int queryTextCount(@Param("text") ManagementText managementText);
    /**
     * 添加公告
     * @param managementText
     */
    int addPQ(@Param("text")ManagementText managementText);


    /**
     * 修改公告
     * @param managementText
     */
    int updatePQ(@Param("text") ManagementText managementText);

    /**
     * 删除公告
     * @param id
     * @return
     */
    int deletePQ(String id);

    int addVip(@Param("user") User user);

    Date queryVip(@Param("user") User user,@Param("num")int month);
}

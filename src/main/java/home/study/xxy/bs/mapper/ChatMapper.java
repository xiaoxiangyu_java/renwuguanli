package home.study.xxy.bs.mapper;


import home.study.xxy.bs.entity.Chat;
import home.study.xxy.bs.entity.CountDay;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface ChatMapper {

    /**
     * 查询通知
     * @param userId
     * @return
     */
    List<Chat> queryChatById(@Param("userId")String userId);

}

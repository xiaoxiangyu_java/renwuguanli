package home.study.xxy.bs.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import home.study.xxy.bs.entity.ComJob;
import home.study.xxy.bs.entity.Page;
import home.study.xxy.bs.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
public interface JobMapper extends BaseMapper<ComJob> {


    /**
     * 查询所有任务
     * */
    List<ComJob> queryAll(@Param("comJob") ComJob comJob,@Param("pageVo") Map pageVo);
    /**
     * 查询总条数
     * */
    int queryJobCount(@Param("comJob") ComJob comJob);

    /**
     * 查询总条数
     * */
    List queryUserJobCount();


    /**
     * 根据查询总条数
     * */
    int queryJobCountById(@Param("comJob") ComJob comJob);
    /**
     * 根据用户id查询所有任务模糊搜索时间
     * */
//    List<ComJob> queryJobById(@Param("comJob") ComJob comJob, @Param("pageVo") Map pageVo,@Param("startTime") Date startTime,@Param("endTime") Date endTime);

    /**
     * 根据用户id查询所有任务
     * */
    List<ComJob> queryJobById(@Param("comJob") ComJob comJob, @Param("pageVo") Map pageVo);
    /**
     * 根据接收者id查询所有任务
     * */
    List<ComJob> queryJobByStaffId(@Param("comJob") ComJob comJob,@Param("pageVo") Map pageVo);
    /**
     * 根据用户id查询总条数
     * */
    int queryJobByIdCount(@Param("comJob") ComJob comJob);

    /**
     * 根据用户id查询总条数模糊搜索时间
     * */
//    int queryJobByIdCount(@Param("comJob") ComJob comJob,@Param("startTime") Date startTime,@Param("endTime") Date endTime);

    /**
     * 添加任务
     * */
    int addJob(@Param("comJob") ComJob comJob);

    /**
     * 分配任务
     * @param comJob
     * @return
     */
    int assJob(@Param("comJob") ComJob comJob);

    /**
     * 修改任务状态
     * */
    int changeJobState(@Param("comJob") ComJob comJob);

    /**
     * 通过任务id删除任务
     * @param jobId
     * @return
     */
    int deleteJob(@Param("jobId") String jobId);

    /**
     * 查询所有未接任务
     */
    List<Map> queryNoAllocationAll();


    /**
     * 全部员工 及 接受的任务
     * @return
     */
    List<Map> queryWorkDate();
}

package home.study.xxy.bs.controller;

import javax.websocket.server.PathParam;

import home.study.xxy.bs.assignment.Assignment;
import home.study.xxy.bs.assignment.ShortTimeAss;
import home.study.xxy.bs.entity.CodeEnum;
import home.study.xxy.bs.entity.ComJob;
import home.study.xxy.bs.entity.Page;
import home.study.xxy.bs.entity.ResultList;
import home.study.xxy.bs.entity.ResultObj;
import home.study.xxy.bs.mapper.UserRoleMapper;
import home.study.xxy.bs.service.JobService;
import home.study.xxy.bs.utils.SpringContextUtils;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import springfox.documentation.service.ResponseMessage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;

/**
 * @title: JobController
 * @功能:  任务管理接口
 * @Author Xiao Xiangyu
 * @Date: 2021/4/18 15:48
 * @Version 1.0
 */
@Slf4j
@Api(value = "任务控制层",tags = {"任务"})
@RestController
@RequestMapping("/jobController")
public class JobController {

    @Autowired
    private JobService jobService;

    @Autowired
    private UserRoleMapper userRoleMapper;




    /*
    * 查询所有任务
    * */
    @ApiOperation(value = "查询所有任务", httpMethod = "POST", notes = "分页查询所有任务")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "失败", response = ResponseMessage.class)})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "comJob", value = "查询任务条件", required = false, dataType = "ComJob"),
            @ApiImplicitParam(name = "pageVo", value = "分页参数", required = false, dataType = "Page"),
    })
    @GetMapping("/queryAll/{comJob}/{index}/{size}")
    public ResultObj queryAllJob(@PathParam("comJob") ComJob comJob,@PathParam("index") int index,@PathParam("size") int size){
        return jobService.queryAllJob(comJob,index,size);
    }

    /*
    * 查询所有人接的任务数
    * */
    @GetMapping("/queryAllUserJob")
    ResultObj queryUserJobCount(){
        return jobService.queryUserJobCount();
    }


    /*
     * 使用id查询接的任务数
     * */
    @GetMapping("/beforeChangeJobState")
    boolean beforeChangeJobState(ComJob comJob){
        if (jobService.queryJobCountById(comJob)>=5){
            return false;
        }else{
            return true;
        }
    }

    /*
     * 使用任务id删除任务
     * */
    @GetMapping("/deleteJob")
    ResultObj deleteJob(String jobId){
        return  jobService.deleteJob(jobId);

    }

    /*
    * 修改任务状态
    * */
    @GetMapping("/changeJobState")
    ResultObj changeJobState(ComJob comJob){
        return jobService.changeJobState(comJob);
    }

    /*
    * 添加任务
    * */
    @PostMapping("/addJob")
    ResultObj addJob(@RequestBody ComJob comJob){
        if (comJob!=null){
            comJob.setCreateTime(new Date());
        }
        if (comJob.getJobType()==null || comJob.getJobType().trim().length()<1){
            comJob.setJobType("网站构建");
        }
        return jobService.addJob(comJob);

    }

    /*
     * 修改任务
     * */
    @PostMapping("/updateJob")
    ResultObj updateJob(@RequestBody ComJob comJob){

        return jobService.updateJob(comJob);

    }

    @InitBinder
    public void initBinder(WebDataBinder binder, WebRequest request) {
        DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

        /**
         * 根据用户id分页查询任务
         * */
    @GetMapping("/queryJobById")
    ResultObj queryJobById(ComJob comJob, Page pageVo){
        if (comJob.getUserId()==null){
            log.error("用户id为空查询失败");
            return new ResultObj(false, CodeEnum.REQUEST_CODE_QUERY_FAIL.getCode(),
                    CodeEnum.REQUEST_CODE_QUERY_FAIL.getMessage());
        }else{
            return jobService.queryJobById(comJob,pageVo);
        }
    }
    /**
     * 根据接取人id分页查询任务
     * */
    @GetMapping("/queryJobByStaffId")
    ResultObj queryJobByStaffId(ComJob comJob, Page pageVo){
        if (comJob.getStaffId()==null){
            log.error("接任务id为空查询失败");
            return new ResultObj(false, CodeEnum.REQUEST_CODE_QUERY_FAIL.getCode(),
                    CodeEnum.REQUEST_CODE_QUERY_FAIL.getMessage());
        }else{
            return jobService.queryJobByStaffId(comJob,pageVo);
        }
    }

    /**
     * 任务智能分配
     */
    @GetMapping("/jobTask")
    ResultObj shortTimeTaskAllocation(){
        jobService.shortTimeTaskAllocation();
        return null;
    }


}

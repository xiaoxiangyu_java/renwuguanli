package home.study.xxy.bs.controller;

import home.study.xxy.bs.entity.ComJob;
import home.study.xxy.bs.entity.Order;
import home.study.xxy.bs.entity.Page;
import home.study.xxy.bs.entity.ResultList;
import home.study.xxy.bs.entity.ResultObj;
import home.study.xxy.bs.service.JobService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @title: orderController
 * @功能: 订单接口
 * @Author Xiao Xiangyu
 * @Date: 2021/4/19 21:43
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/orderController")
public class OrderController {
    @Autowired
    private JobService jobService;

    /**
     * 根据id查询所有订单
     * */
    @GetMapping("queryOrderById")
    public ResultObj queryAllJob(ComJob comJob, Page pageVo){
        return jobService.queryJobById(comJob, pageVo);
    }
}

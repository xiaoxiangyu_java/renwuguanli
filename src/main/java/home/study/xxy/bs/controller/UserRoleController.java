package home.study.xxy.bs.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * 类描述：
 *   前端控制器
 *
 * @author xiaoxiangyu
 * @date 2022-05-17 16:14
 */
@Controller
@RequestMapping("/test/user-role")
public class UserRoleController {

}

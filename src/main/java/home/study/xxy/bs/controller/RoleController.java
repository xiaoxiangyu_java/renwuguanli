package home.study.xxy.bs.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * 类描述：
 *   前端控制器
 *
 * @author chenjs
 * @date 2022-05-17 15:57
 */
@Controller
@RequestMapping("/security/role")
public class RoleController {

}

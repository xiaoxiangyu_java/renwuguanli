package home.study.xxy.bs.controller;

import com.alibaba.fastjson.JSONObject;
import home.study.xxy.bs.constans.Constants;
import home.study.xxy.bs.entity.*;
import home.study.xxy.bs.service.LoginService;
import home.study.xxy.bs.service.OtherService;
import home.study.xxy.bs.utils.*;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.service.ResponseMessage;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @title: LoginController
 * @功能:
 * @Author Xiao Xiangyu
 * @Date: 2021/4/11 19:04
 * @Version 1.0
 */
@Slf4j
@Api(value = "登录管理控制器类", tags = {"登录管理"})
@RestController
public class LoginController extends BaseController{

    private StringBuffer rightString;
    @Autowired
    private LoginService loginService;

    @Autowired
    private OtherService otherService;



    @ApiOperation(value = "test", httpMethod = "POST", notes = "test")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "fail", response = ResponseMessage.class)})
    @RequestMapping(value = "/test")
    public String test(){
//        FfmpegUtils.playVideoAudio("F:\\xxy\\workspace\\red5_hls-master\\red5_hls-master\\webapps\\oflaDemo\\streams\\file.flv");
        System.out.println("test   ...");
        return "test ok";
    }

    /**
     * 登录接口
     */
    @ApiOperation(value = "用户登录", httpMethod = "POST", notes = "用户通过平台登录，同个账号可以登录多次，返回个人信息")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "失败", response = ResponseMessage.class)})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user", value = "登录信息", required = false, dataType = "User"),
            @ApiImplicitParam(name = "number", value = "验证码", required = false, dataType = "String"),
            @ApiImplicitParam(name = "session", value = "暂没用", required = false, dataType = "HttpSession"),
    })
    @RequestMapping(value = "/login")
    public ResultObj loginController(@RequestBody User user, String number, HttpSession session) {
//        if (number == null) {
//            return null;
//        }
//        if (number.equalsIgnoreCase(rightString.toString())) {
//            if (loginService.checkLogin(user) >= 1) {
                ResultObj loginData = loginService.getLoginData(user);

                log.info(user.getUserName() + "  登录成功  IP:" + NetUtils.getIPAddress(request));
                return loginData;
//            } else {
//                log.error("登录验证失败");
//            }
//        } else {
//            log.error("验证码错误");
//        }
//        return null;
    }

    /**
     * 查询所有用户
     */
    @ApiOperation(value = "查询所有用户", httpMethod = "POST", notes = "查询所有用户个人信息")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "失败", response = ResponseMessage.class)})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user", value = "登录信息", required = false, dataType = "user"),
            @ApiImplicitParam(name = "pageVo", value = "验证码", required = false, dataType = "Page"),
    })
    @RequestMapping("/getAll")
    public ResultList<User> queryAllUser(User user, Page pageVo) {
        return loginService.queryAll(user, pageVo);
    }

    /**
     * 新增注册用户
     */
    @ApiOperation(value = "新增注册用户", httpMethod = "POST", notes = "新增注册用户个人信息")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "失败", response = ResponseMessage.class)})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user", value = "登录信息", required = false, dataType = "user"),
    })
    @RequestMapping("/addUser")
    public ResultObj addUser( User user) {
        if (user == null) {
            return ResultObj.fail(CodeEnum.REQUEST_CODE_REGISTER_FAIL);
        }
        int i = loginService.addUser(user);
        if (i == 1){
            log.info("注册成功  "  + NetUtils.getIPAddress(request));
            return ResultObj.ok(CodeEnum.REQUEST_CODE_REGISTER_SUCCESS);
        }else{
            return ResultObj.fail(CodeEnum.REQUEST_CODE_REGISTER_FAIL);
        }

//        if (){
//
//        }else{
//            log.info("注册失败");
//            return false;
//        }

    }

    /**
     * 修改密码，密码确认
     */
//    @GetMapping("/beforeChangePwd")
//    public boolean beforeChangePwd(String pwd) {
//        if (MD5Utils.stringToMD5(pwd).equals(pwdsession)){
//            return true;
//        }
//        return false;
//    }
    /**
     * 修改用户信息
     */
    @ApiOperation(value = "修改用户信息", httpMethod = "POST", notes = "修改用户个人信息")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "失败", response = ResponseMessage.class)})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user", value = "登录信息", required = false, dataType = "user"),
            @ApiImplicitParam(name = "vipTimeString", value = "验证码", required = false, dataType = "String"),
    })
    @RequestMapping("/changePwd")
    public ResultObj changePwd(User user,String vipTimeString) {
        if (user == null) {
            return ResultObj.fail(CodeEnum.REQUEST_CODE_UPDATE_FAIL);
        }
        if (vipTimeString!=null){

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");//
            try {
                Date date = simpleDateFormat.parse(vipTimeString);
                user.setVipTime(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        int result = loginService.changePwd(user);
        if (result == 1) {
            log.info("修改信息成功  " + NetUtils.getIPAddress(request));
            return ResultObj.ok(CodeEnum.REQUEST_CODE_UPDATE_SUCCESS);
        } else {
            log.info("修改信息失败  " + NetUtils.getIPAddress(request));
            return ResultObj.fail(CodeEnum.REQUEST_CODE_UPDATE_FAIL);
        }

    }

    /**用户购买vip
     *
     * @return
     */
    @ApiOperation(value = "用户购买vip", httpMethod = "POST", notes = "修改用户个人信息 用户购买vip")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "失败", response = ResponseMessage.class)})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user", value = "登录信息", required = false, dataType = "User"),
            @ApiImplicitParam(name = "vipAddMonth", value = "验证码", required = false, dataType = "String"),
    })
    @RequestMapping("/addVip")
    public ResultObj addVip(@RequestBody User user,String vipAddMonth) {
        if (user == null) {
            return ResultObj.fail(CodeEnum.REQUEST_CODE_UPDATE_FAIL);
        }
        int num = Integer.parseInt(vipAddMonth);
        int result = otherService.addVip(user,num);

//        if (vipTimeString!=null){
//
//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");//
//            try {
//                Date date = simpleDateFormat.parse(vipTimeString);
//                user.setVipTime(date);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//        }
//        int result = loginService.addVip(user);
        if (result == 1) {
            log.info("修改信息成功  " + NetUtils.getIPAddress(request));
            return ResultObj.ok(CodeEnum.REQUEST_CODE_UPDATE_SUCCESS);
        } else {
            log.info("修改信息失败  " + NetUtils.getIPAddress(request));
            return ResultObj.fail(CodeEnum.REQUEST_CODE_UPDATE_FAIL);
        }
    }

    /**
     * 修改用户权限
     */
    @ApiOperation(value = "修改用户权限", httpMethod = "POST", notes = "修改用户权限 用户购买vip")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "失败", response = ResponseMessage.class)})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user", value = "登录信息", required = false, dataType = "User"),
    })
    @RequestMapping("/updateAdmin")
    public ResultObj updateAdmin(@RequestBody User user) {
        if (user == null) {
            return ResultObj.fail(CodeEnum.REQUEST_CODE_UPDATE_FAIL);
        }
        int result = loginService.updateAdmin(user);
        if (result == 1) {
            log.info("修改权限成功  " + NetUtils.getIPAddress(request));
            return ResultObj.ok(CodeEnum.REQUEST_CODE_UPDATE_SUCCESS);
        } else {
            log.info("修改权限失败  " + NetUtils.getIPAddress(request));
            return ResultObj.fail(CodeEnum.REQUEST_CODE_UPDATE_FAIL);
        }

    }

    /**
     * 验证码生成
     */
    @ApiOperation(value = "验证码生成", httpMethod = "POST", notes = "验证码生成")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "失败", response = ResponseMessage.class)})
    @RequestMapping(value = "/checkImage")
    public void CheckImageUtils(HttpServletResponse response) {
        CheckImageUtils imageUtils = new CheckImageUtils();
        try {
            ImageIO.write(imageUtils.dre(), "jpg", response.getOutputStream());
            rightString = imageUtils.getSb();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            log.error("验证码生成失败  " + NetUtils.getIPAddress(request));
            e.printStackTrace();
        }
    }
    /**
     * 导出用户数据
     */
    @ApiOperation(value = "导出用户数据", httpMethod = "POST", notes = "导出用户数据")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "失败", response = ResponseMessage.class)})
    @PostMapping(value = "/exportUser")
    public ResultObj ExportUsers(){
        try {
            String token = request.getHeader(Constants.TOKEN_PARAM_KEY);
            String id = JWTUtil.getUsername(token);
            LoginUser user1 = JSONObject.parseObject(String.valueOf(RedisUtils.getInstance().get("login:" + token)), LoginUser.class);
            User user = user1.getUser();
            log.error("用户 " + user.getUserName() +"  导出用户数据  " + NetUtils.getIPAddress(request));

            List<User> all = loginService.findAll();
            ExcelUtils.export(response,"user",all,User.class);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultObj.fail(CodeEnum.REQUEST_CODE_EXPORT_FAIL);
        }

        return ResultObj.ok(CodeEnum.REQUEST_CODE_EXPORT_SUCCESS);
    }

    /**
     * 导入用户数据
     */
    @ApiOperation(value = "导入用户数据", httpMethod = "POST", notes = "导入用户数据")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "失败", response = ResponseMessage.class)})
    @PostMapping(value = "/importUser")
    public ResultObj ImportUser(MultipartFile file) {
        try {
            List<User> users = ExcelUtils.readMultipartFile(file, User.class);
            for (User user : users) {
                System.out.println(user.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
            return ResultObj.fail(CodeEnum.REQUEST_CODE_IMPORT_FAIL);
        }

        return ResultObj.ok(CodeEnum.REQUEST_CODE_IMPORT_SUCCESS);
    }


}

package home.study.xxy.bs.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import home.study.xxy.bs.domain.Server;
import home.study.xxy.bs.entity.*;
import home.study.xxy.bs.service.JobService;
import home.study.xxy.bs.service.OtherService;
import home.study.xxy.bs.utils.HttpClientUtils;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * @title: OtherController
 * @功能:
 * @Author Xiao Xiangyu
 * @Date: 2021/5/1 16:02
 * @Version 1.0
 */
@Slf4j
@Api(value = "公告和统计控制层",tags = {"公告和统计"})
@RestController
@RequestMapping("/otherController")
public class OtherController {


    @Autowired
    private OtherService otherService;

    @Autowired
    private JobService jobService;

    @GetMapping("/getCountSum")
    public ResultObj countSum(){
        return otherService.countSum();
    }

    @GetMapping("/addPQ")
    public ResultObj addPQ(ManagementText managementText){
        return otherService.addPQ(managementText);
    }
    @GetMapping("/delete")
    public ResultObj deletePQ(String id){
       return otherService.deletePQ(id);

    }

    @GetMapping("/updatePQ")
    public ResultObj updatePQ(ManagementText managementText){
         managementText.setCreateTime(null);
        return otherService.updatePQ(managementText);

    }


    @GetMapping("/queryPQ")
    public ResultObj queryPQ(ManagementText managementText,Page pageVo){
        return otherService.queryPQ(managementText, pageVo);
    }


    @GetMapping("/export/{ids}")
    public ResultObj export(@PathVariable ArrayList<Integer> ids){
        int size = 0;
        if (ids == null){
            return new ResultObj(true,CodeEnum.REQUEST_CODE_EXPORT_FAIL.getCode(),CodeEnum.REQUEST_CODE_EXPORT_FAIL.getMessage());
        }
        size = ids.size();
        if (size > 0 )
        {
            List<ComJob> byIds = jobService.findByIds(ids);
            if (byIds != null){

            }
        }

        return null;
    }

    @GetMapping("/weather")
    public ResultObj weather(){

        ResultObj ok = null;
        try {
            String url = "https://api.caiyunapp.com/v2.6/TAkhjf8d1nlSlspN/104.066541,30.572269/realtime";
            String we = HttpClientUtils.doGet(url);
            WeatherInfo weatherInfo = JSON.parseObject(JSON.parseObject(JSON.parseObject(we).get("result").toString()).get("realtime").toString(),WeatherInfo.class);
            ok = ResultObj.ok(CodeEnum.REQUEST_CODE_QUERY_SUCCESS);

            ok.setData(weatherInfo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultObj.fail(CodeEnum.REQUEST_CODE_QUERY_FAIL);
        }
        return ok;
    }


    @GetMapping("/info")
    public ResultObj getInfo() throws Exception
    {
        Server server = new Server();
        server.copyTo();
        ResultObj ok = ResultObj.ok();
        ok.setData(server);
        return ok;
    }

}

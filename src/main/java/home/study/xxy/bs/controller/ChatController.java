package home.study.xxy.bs.controller;

import javax.annotation.Resource;

import home.study.xxy.bs.entity.Bill;
import home.study.xxy.bs.entity.Chat;
import home.study.xxy.bs.entity.CodeEnum;
import home.study.xxy.bs.entity.Page;
import home.study.xxy.bs.entity.ResultList;
import home.study.xxy.bs.entity.ResultObj;
import home.study.xxy.bs.mapper.ChatMapper;
import home.study.xxy.bs.msg.WebSocketSever;
import home.study.xxy.bs.service.BillService;
import home.study.xxy.bs.service.ChatService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @title: BillController
 * @功能:
 * @Author Xiao Xiangyu
 * @Date: 2021/4/23 21:18
 * @Version 1.0
 */

@Slf4j
@RestController
@RequestMapping("/chatController")
public class ChatController {
    @Autowired
    private ChatService chatService;

    @Resource
    private WebSocketSever socket;

    /**
     * 查询所有通知
     * @param userId
     * @return
     */
    @GetMapping("/queryAllById")
    public ResultObj queryChatById(String userId){
        List<Chat> chats = chatService.queryChatById(userId);
        if (chats!=null&&chats.size()>0){
            return new ResultObj(true, CodeEnum.REQUEST_CODE_QUERY_SUCCESS.getCode(),CodeEnum.REQUEST_CODE_QUERY_SUCCESS.getMessage(),chats);
        }else{
            return new ResultObj(true, CodeEnum.REQUEST_CODE_QUERY_FAIL.getCode(),CodeEnum.REQUEST_CODE_QUERY_FAIL.getMessage());
        }
    }

    @GetMapping("/sendToUser")
    public void sendMsg(@RequestParam("id") Integer id,@RequestParam("msg") String msg){
        try {
            WebSocketSever.sendMessageByUser(id,msg);
        } catch (Exception e) {
            e.printStackTrace();
//            return new ResultObj(true, CodeEnum.REQUEST_CODE_SEND_FAIL.getCode(),CodeEnum.REQUEST_CODE_SEND_FAIL.getMessage());
        }
//        return new ResultObj(true, CodeEnum.REQUEST_CODE_SEND_SUCCESS.getCode(),CodeEnum.REQUEST_CODE_SEND_SUCCESS.getMessage());
    }
}

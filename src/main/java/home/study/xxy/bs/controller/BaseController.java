package home.study.xxy.bs.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * 统一控制层  请求体
 */
public abstract class BaseController {

    @Resource
    protected HttpServletRequest request;


    @Resource
    protected HttpServletResponse response;



}
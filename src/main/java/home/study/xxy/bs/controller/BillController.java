package home.study.xxy.bs.controller;

import home.study.xxy.bs.entity.Bill;
import home.study.xxy.bs.entity.Order;
import home.study.xxy.bs.entity.Page;
import home.study.xxy.bs.entity.ResultList;
import home.study.xxy.bs.entity.ResultObj;
import home.study.xxy.bs.service.BillService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @title: BillController
 * @功能:
 * @Author Xiao Xiangyu
 * @Date: 2021/4/23 21:18
 * @Version 1.0
 */

@Slf4j
@RestController
@RequestMapping("/billController")
public class BillController {
    @Autowired
    private BillService billService;

    /**
     * 根据用户id查询所有账单
     * */
    @GetMapping("queryAllBillById")
    public ResultObj queryAllBill(Bill bill, Page pageVo){
        return billService.queryAllBill(bill,pageVo);
    }
}

package home.study.xxy.bs.handler;

import com.alibaba.fastjson.JSON;
import home.study.xxy.bs.entity.ResultObj;
import home.study.xxy.bs.utils.WebUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author xiaoxiangyu
 * @create 2022-05-26 23:30
 * @功能：
 */
@Component
@Slf4j
public class AuthenFailImpl implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        ResultObj result = ResultObj.AuthenFail();
        String json = JSON.toJSONString(result);
        WebUtil.renderString(response,json);

        log.info("请求地址: "+ request.getRequestURI() + "  " + json);

    }
}

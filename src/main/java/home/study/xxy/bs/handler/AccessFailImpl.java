package home.study.xxy.bs.handler;

import com.alibaba.fastjson.JSON;
import home.study.xxy.bs.entity.ResultObj;
import home.study.xxy.bs.utils.WebUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author xiaoxiangyu
 * @create 2022-05-26 23:30
 * @功能：
 */
@Component
@Slf4j
public class AccessFailImpl implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        ResultObj result = ResultObj.AuthenFail();
        String json = JSON.toJSONString(result);
        WebUtil.renderString(response,json);

        log.info(json);

    }

}

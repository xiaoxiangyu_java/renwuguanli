package home.study.xxy.bs.fifter;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * @author xiaoxiangyu
 * @create 2022-05-17 0:59
 * @功能：
 */
@Slf4j
@Aspect
@Component
public class TestAop {


    @Before("execution(public * home.study.xxy.bs.controller.LoginController.*(..))")
    public void test(){
        log.info("==============================LoginController==============================");
    }

}

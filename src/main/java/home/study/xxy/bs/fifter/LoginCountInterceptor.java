package home.study.xxy.bs.fifter;

import home.study.xxy.bs.mapper.LoginMapper;
import home.study.xxy.bs.mapper.OtherMapper;
import home.study.xxy.bs.service.OtherService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @title: UserInterceptor
 * @功能: 自定义拦截器  登录用户统计
 * @Author Xiao Xiangyu
 * @Date: 2021/5/1 13:39
 * @Version 1.0
 */
@Slf4j
@Component
public class LoginCountInterceptor implements HandlerInterceptor {

    @Autowired
    private OtherService otherService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        return true;

    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        Date date = new Date();
        otherService.addCount(date);
    }
}

package home.study.xxy.bs.fifter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @title: WebAppConfigurer
 * @功能: 自定义拦截器配置
 * @Author Xiao Xiangyu
 * @Date: 2021/5/1 13:49
 * @Version 1.0
 */
@Configuration
public class WebAppConfigurer implements WebMvcConfigurer {

//    @Autowired
//    private UserInterceptor userInterceptor;
//
//    @Autowired
//    private LoginCountInterceptor loginCountInterceptor;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOriginPatterns("*")
                .allowedMethods("GET","POST","PUT","DELETE","HEAD","OPTIONS")
                .allowCredentials(true)
                .maxAge(3600)
                .allowedHeaders("*");
    }


    //    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
////        registry.addInterceptor(userInterceptor).addPathPatterns("/**");
////                .excludePathPatterns("/login")
////                .excludePathPatterns("/addUser")
////                .excludePathPatterns("/checkImage");
////        registry.addInterceptor(loginCountInterceptor).addPathPatterns("/login");
//
//
//    }
}

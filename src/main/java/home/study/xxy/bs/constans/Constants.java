package home.study.xxy.bs.constans;

public final class Constants {

    /**
     * 空字符串
     */
    public static final String EMPTY_STR = "";

    /***
     * 以参数方式传递token的参数key
     */
    public static final String TOKEN_PARAM_KEY = "token";



    /**
     * AES加密KEY
     */
    public final static String AES_KEY = "799126a605235a45";

    /**
     * AES加密向量
     */
    public final static String AES_IV = "ef70bd83506f4907";

    /**
     * 一键登录RSA解密私钥（一键登录服务商提供）
     */
    public final static String RSA_PRIVATE_KEY = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBANOSkO6CFHG6IicP" +
            "cIgR37SZfu6eOBVych+aO6h9oUWyIAnCburf7IrlLu8aRyq4GSCOk9iBH6BRAbSb" +
            "zplvHu4hWyUu5BjLZqXZNosIY92xFpSGxGVeRFs0tDNOdgEB9PLPjYqlTPDjnOMi" +
            "TUYa2kylczQAeb79SEzeGEXmyn1fAgMBAAECgYBKVn4r/3KVfxQO62LbvIQAnco2" +
            "p0sHZh3pMrTxf6rRpUOSikmUpzcimmJCStZkkSjrAo26DayCFNRnlDMkQCH6X7NE" +
            "RPPJMut5IrtqvZDI31B05WEFmUbdL8evG3nvtNKU9M9ODV4HxwwBPjhIAXfhMSHI" +
            "WJuC5NGQWKeFzeYIAQJBAPnSm1VIVqg8BYteBXCUa+HCNbrYPsmeSHcPvBB7umgJ" +
            "qEyvzMGj+cjJanA9LY9ixXLYcbNqVa8lhLXc8XO+VAECQQDYzdVeNHFcmkFWTMsW" +
            "hTstjo5s3qeJ+2TblT9R421ytHOTD7GWA+o64UD5BPFQd92L2BPA9eQxb9LQ+Q3W" +
            "lVFfAkAEjiWM8Yu695U2TFmXCu3IqQznwkeqz3sMyLmqJ0dn3D04YMBAC6hjU+Df" +
            "+Q2anrdw96djGR5E2k9ILaYtcRwBAkAB7pOgK0d3U8iRRE3q/XTxivup7LIBgMP9" +
            "UO8ng6dOBl2hSdqDQ7WqoKc7kNe1p9vJizth1M8bwatXQWDmMJ5xAkBQeIj3t2zG" +
            "33Q7XvbwgFhn+CohJHFv9dj1C399V9veL8UK5VAHohOAu9Pf+T5d87jubJAFSl5y" +
            "Xsb4coADm2Ec";



}
package home.study.xxy.bs.config;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @Classname ConfigParam
 * @Description
 * @Date 2021/7/29 15:09
 * @Created by huangh@cosber.com
 */
@Component
@ConfigurationProperties(value = "job")
@PropertySource(value = {"classpath:config_param.properties"})
@AllArgsConstructor
@NoArgsConstructor
public class ConfigParam implements Serializable {
    @Value("${job.token-timeout}")
    private Long tokenTimeout;

    @Value("${job.file.upload.path}")
    private String uploadPath;

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    public Long getTokenTimeout() {
        return tokenTimeout;
    }

    public void setTokenTimeout(Long tokenTimeout) {
        this.tokenTimeout = tokenTimeout;
    }
}

package home.study.xxy.bs.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import home.study.xxy.bs.entity.ResultObj;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.connector.Response;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author xiaoxiangyu
 * @create 2022-05-17 19:56
 * @功能：
 */
@Slf4j
@Component("loginSuccessHandler")
public class LoginSuccessHandler implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        //返回json 提示
        ResultObj success = ResultObj.ok().message("登录成功");
        response.setContentType("text/json;charset=utf-8");
        response.getWriter().println(new ObjectMapper().writeValueAsString(success));
//        response.getOutputStream().println(new ObjectMapper().writeValueAsString(response));
    }
}

package home.study.xxy.bs.msg;

import javax.annotation.Resource;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

import ch.qos.logback.core.joran.spi.NoAutoStart;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import home.study.xxy.bs.entity.Msg;
import home.study.xxy.bs.entity.User;
import home.study.xxy.bs.mapper.LoginMapper;
import home.study.xxy.bs.service.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@ServerEndpoint("/websocket/{userId}")
@Component
@Slf4j
public class WebSocketSever {

    // 与某个客户端的连接会话，需要通过它来给客户端发送数据
    private Session session;


    private static LoginService loginService;

    // session集合,存放对应的session
    private static ConcurrentHashMap<Integer, Session> sessionPool = new ConcurrentHashMap<>();

    // concurrent包的线程安全Set,用来存放每个客户端对应的WebSocket对象。
    private static CopyOnWriteArraySet<WebSocketSever> webSocketSet = new CopyOnWriteArraySet<>();

    //延时消息集合  消息队列？



    //直接@Autowired spring服务为单例 而不同聊天连接是不同的websocket 与用户为多对多的关系
    //所以spring开发者对于websocket使用了java原生的机制进行管理
    //websocket对象不是由spring创建的  就没有进行依赖注入
    //
    @Autowired
    public void setLoginService(LoginService loginService) {
        WebSocketSever.loginService = loginService;
    }



    /**
     * 建立WebSocket连接
     *
     * @param session
     * @param userId 用户ID
     */
    @OnOpen
    public void onOpen(Session session, @PathParam(value = "userId") Integer userId) {
        log.info("WebSocket建立连接中,连接用户ID：{}", userId);
        try {
            Session historySession = sessionPool.get(userId);
            // historySession不为空,说明已经有人登陆账号,应该删除登陆的WebSocket对象
            if (historySession != null) {
                webSocketSet.remove(historySession);
                historySession.close();
            }
        } catch (IOException e) {
            log.error("重复登录异常,错误信息：" + e.getMessage(), e);
        }
        // 建立连接
        this.session = session;
        webSocketSet.add(this);
        sessionPool.put(userId, session);
        log.info("建立连接完成,当前在线人数为：{}", webSocketSet.size());
    }

    /**
     * 发生错误
     *
     * @param throwable e
     */
    @OnError
    public void onError(Throwable throwable) {
        throwable.printStackTrace();
    }

    /**
     * 连接关闭
     */
    @OnClose
    public void onClose() {
        webSocketSet.remove(this);
        log.info("连接断开,当前在线人数为：{}", webSocketSet.size());
    }

    /**
     * 接收客户端消息
     *
     * @param message 接收的消息
     */
    @OnMessage
    public void onMessage(String message) {
        String userId = session.getPathParameters().get("userId");
        Msg msg = JSONObject.parseObject(message, Msg.class);
        if (msg == null){
            return;
        }

        System.out.println(msg.toString());
        String name = "";
        if (loginService != null){
            User user = loginService.findOne(msg.getId()+"");
            if ( user != null ){
                name =  user.getUserName();
            }
        }
        log.info("用户" + userId + "收到客户端发来的消息：{} 发给用户"+msg.getId()+" "+name, msg.getMsg());

        //检测是否用户是否在连接里  发送消息
        sendMessageByUser(Integer.valueOf(userId),msg.getId(),msg.getMsg());


    }

    /**
     * 推送消息到指定用户
     *
     * @param userId  用户ID
     * @param message 发送的消息   在线直接发送   离线加入定时任务？或者建立连接去查数据缓存
     */
    public static void sendMessageByUser(Integer userId, String message) {
        sendMessageByUser(null,userId,message);
    }
    public static void sendMessageByUser(Integer sendId,Integer userId, String message) {
        if (userId == null){
            log.info("推送消息到指定用户未连接：" );
            return;
        }
        Session session = sessionPool.get(userId);
        try {
            if (session!=null){
                Msg msg = new Msg();
                msg.setMsg(message);
                User user = loginService.findOne(userId+"");
                User user1 = loginService.findOne(sendId+"");

                msg.setUserNick(user.getUserName());
                msg.setUser1Id(user.getId());
                msg.setUser1Id(user1.getId());
                msg.setUser1Nick(user1.getUNick());

                message = JSON.toJSONString(msg);
                session.getBasicRemote().sendText(message);
                log.info(sendId + "发送到指定用户：" + userId +" 消息 "+ message );
            }else{
                log.info("推送消息到指定用户未连接：" );
            }
            //离线处理
        } catch (IOException e) {
            log.error("推送消息到指定用户发生错误：" + e.getMessage(), e);
        }
    }

    /**
     * 群发消息
     *
     * @param message 发送的消息
     */
    public static void sendAllMessage(String message) {
        log.info("发送消息：{}", message);
        for (WebSocketSever webSocket : webSocketSet) {
            try {
                webSocket.session.getBasicRemote().sendText(message);
            } catch (IOException e) {
                log.error("群发消息发生错误：" + e.getMessage(), e);
            }
        }
    }

}
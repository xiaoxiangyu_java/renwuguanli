package home.study.xxy.bs.msg;

public class WebSocketVO {

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 人才ID
     */
    private Integer talentsId;

    public WebSocketVO() {
    }

    public WebSocketVO(Integer userId, Integer talentsId) {
        this.userId = userId;
        this.talentsId = talentsId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getTalentsId() {
        return talentsId;
    }

    public void setTalentsId(Integer talentsId) {
        this.talentsId = talentsId;
    }

    @Override
    public String toString() {
        return "WebSocketVO{" +
                "userId=" + userId +
                ", talentsId=" + talentsId +
                '}';
    }


}

//vue 前端demo
//<template>
//<el-container>
//<el-header>
//<el-col :span="12">聊天</el-col>
//<el-col :span="6"><el-input v-model="as" placeholder="请输入内容"></el-input></el-col>
//<el-col :span="6"><el-button size="mini" type="primary" @click="initWebSocket(as) ">连接</el-button></el-col>
//<el-input v-model="toUserId" placeholder="发送给"></el-input>
//</el-header>
//<el-main>
//<ul class="infinite-list" v-infinite-scroll="load" style="overflow:auto">
//<li v-for="i in list" class="infinite-list-item" style="list-style: none;">{{ i.userNick+":"+  i.msg }}</li>
//</ul>
//
//</el-main>
//<el-footer>
//<el-col :span="12"><el-input v-model="input" placeholder="请输入内容"></el-input></el-col>
//<el-col :span="12"><el-button size="mini" type="primary" @click="websocketsend(input,toUserId) ">发送</el-button></el-col>
//</el-footer>
//</el-container>
//</template>
//
//<script>
//   export default {
//        name : 'test',
//        data() {
//        return {
//        websock: null,
//        input:"",
//        as:0,
//        toUserId:"",
//        list:[
//        ],
//        count:5,
//        }
//        },
//        created() {
//        // this.initWebSocket();
//        },
//        destroyed() {
//        this.websock.close() //离开路由之后断开websocket连接
//        },
//        methods: {
//        load () {
//        //
//        },
//        initWebSocket(data){ //初始化weosocket
//        this.list = [];
//        const wsuri = "ws://127.0.0.1:8888/websocket/"+data;
//        this.websock = new WebSocket(wsuri);
//        this.websock.onmessage = this.websocketonmessage;
//        this.websock.onopen = this.websocketonopen;
//        this.websock.onerror = this.websocketonerror;
//        this.websock.onclose = this.websocketclose;
//        },
//        websocketonopen(){ //连接建立之后执行send方法发送数据
//        let actions = {"test":"连接成功"};
//        this.websock.send(JSON.stringify(actions));
//        },
//        websocketonerror(){//连接建立失败重连
//        this.initWebSocket();
//        },
//        websocketonmessage(e){ //数据接收
//        const redata = JSON.parse(e.data);
//        this.list.push(redata);
//        },
//
//        websocketsend(Data,id){//数据发送
//        let actions = {"msg":Data,"id":id};
//        this.websock.send(JSON.stringify(actions));
//        this.list.push(actions);
//        this.input="";
//        },
//        websocketclose(e){  //关闭
//        console.log('断开连接',e);
//        },
//        },
//        }
//</script>
//
//<style>
//	.el-container{
//            min-height: 100vh;
//            }
//</style>
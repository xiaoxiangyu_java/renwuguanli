package home.study.xxy.bs;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@MapperScan(value = "home.study.xxy.bs.mapper")
@EnableScheduling
public class JobManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(JobManagementApplication.class, args);

        System.out.println("      _/_/_/  _/_/_/_/_/    _/_/    _/_/_/    _/_/_/_/_/  _/    _/  _/_/_/          _/_/_/    _/_/    _/      _/  _/_/_/    _/        _/_/_/_/  _/_/_/_/_/  _/_/_/_/  _/_/_/    \n" +
                "   _/            _/      _/    _/  _/    _/      _/      _/    _/  _/    _/      _/        _/    _/  _/_/  _/_/  _/    _/  _/        _/            _/      _/        _/    _/   \n" +
                "    _/_/        _/      _/_/_/_/  _/_/_/        _/      _/    _/  _/_/_/        _/        _/    _/  _/  _/  _/  _/_/_/    _/        _/_/_/        _/      _/_/_/    _/    _/    \n" +
                "       _/      _/      _/    _/  _/    _/      _/      _/    _/  _/            _/        _/    _/  _/      _/  _/        _/        _/            _/      _/        _/    _/     \n" +
                "_/_/_/        _/      _/    _/  _/    _/      _/        _/_/    _/              _/_/_/    _/_/    _/      _/  _/        _/_/_/_/  _/_/_/_/      _/      _/_/_/_/  _/_/_/        \n");
    }


//    @Scheduled(cron = "0/1 0/1 * * * ? ")
//    public void test(){
//        System.out.println("test");
//    }
}

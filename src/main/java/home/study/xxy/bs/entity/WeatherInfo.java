package home.study.xxy.bs.entity;

import java.util.HashMap;

/**
 * 天气实体类
 */
public class WeatherInfo {

    private String temperature;//	地表 2 米气温
    private String apparentTemperature;//	体感温度
    private String pressure;//	地面气压
    private String humidity;//	地表 2 米湿度相对湿度(%)
    private HashMap wind;

    private HashMap precipitation;

    private String cloudrate;//	总云量(0.0-1.0)
    private String dswrf;//	向下短波辐射通量(W/M2)
    private String visibility;//	地表水平能见度
    private String skycon;//	天气现象

    private HashMap lifeIndex;

    private HashMap airQuality;

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getApparentTemperature() {
        return apparentTemperature;
    }

    public void setApparentTemperature(String apparentTemperature) {
        this.apparentTemperature = apparentTemperature;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public HashMap getWind() {
        return wind;
    }

    public void setWind(HashMap wind) {
        this.wind = wind;
    }

    public HashMap getPrecipitation() {
        return precipitation;
    }

    public void setPrecipitation(HashMap precipitation) {
        this.precipitation = precipitation;
    }

    public String getCloudrate() {
        return cloudrate;
    }

    public void setCloudrate(String cloudrate) {
        this.cloudrate = cloudrate;
    }

    public String getDswrf() {
        return dswrf;
    }

    public void setDswrf(String dswrf) {
        this.dswrf = dswrf;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public String getSkycon() {
        return skycon;
    }

    public void setSkycon(String skycon) {
        this.skycon = skycon;
    }

    public HashMap getLifeIndex() {
        return lifeIndex;
    }

    public void setLifeIndex(HashMap lifeIndex) {
        this.lifeIndex = lifeIndex;
    }

    public HashMap getAirQuality() {
        return airQuality;
    }

    public void setAirQuality(HashMap airQuality) {
        this.airQuality = airQuality;
    }
}

package home.study.xxy.bs.entity;

/**
 * @author xiaoxiangyu
 * @create 2022-05-17 19:23
 * @功能： 状态码 枚举
 */
public enum CodeEnum {

    //200  成功
    // 400 ~ 501 失败

    REQUEST_CODE_SUCCESS("200","成功"),
    REQUEST_CODE_FAIL("400","失败"),
    REQUEST_CODE_NULL("404","资源未找到"),
    REQUEST_CODE_SERVER_FAIL("500","服务器错误"),

    REQUEST_CODE_REGISTER_SUCCESS("200","注册成功"),
    REQUEST_CODE_REGISTER_FAIL("400","注册失败"),

    REQUEST_CODE_QUERY_SUCCESS("200","查询成功"),
    REQUEST_CODE_QUERY_FAIL("400","查询失败"),

    REQUEST_CODE_UPDATE_SUCCESS("200","修改成功"),
    REQUEST_CODE_UPDATE_FAIL("400","修改失败"),

    REQUEST_CODE_ABANDON_SUCCESS("200","放弃成功"),
    REQUEST_CODE_ABANDON_FAIL("400","放弃失败"),

    REQUEST_CODE_DELETE_SUCCESS("200","删除成功"),
    REQUEST_CODE_DELETE_FAIL("400","删除失败"),

    REQUEST_CODE_ADD_SUCCESS("200","添加成功"),
    REQUEST_CODE_ADD_FAIL("400","添加失败，任务审核未通过"),

    REQUEST_CODE_ACCEPT_SUCCESS("200","接受成功"),
    REQUEST_CODE_ACCEPT_FAIL("400","接受失败"),

    REQUEST_CODE_LOGIN_SUCCESS("200","登录成功"),
    REQUEST_CODE_LOGIN_FAIL("400","登录失败"),

    REQUEST_CODE_SEND_SUCCESS("200","发送成功"),
    REQUEST_CODE_SEND_FAIL("400","发送失败"),

    REQUEST_CODE_EXPORT_SUCCESS("200","导出成功"),
    REQUEST_CODE_EXPORT_FAIL("400","导出失败"),

    REQUEST_CODE_IMPORT_SUCCESS("200","导入成功"),
    REQUEST_CODE_IMPORT_FAIL("400","导入失败"),

    REQUEST_CODE_ACCESS_FAIL("401","权限不足"),
    REQUEST_CODE_AUTHEN_FAIL("402","验证错误,登录信息已过期"),


    ;
    public String code;
    public String message;


    CodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

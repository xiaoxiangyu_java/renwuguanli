package home.study.xxy.bs.entity;


import java.util.Date;

public class ManagementText {

  private String textId;
  private String proName;
  private String answer;
  private Date createTime;
  private String show;
  private String textType;


  public String getTextId() {
    return textId;
  }

  public void setTextId(String textId) {
    this.textId = textId;
  }


  public String getProName() {
    return proName;
  }

  public void setProName(String proName) {
    this.proName = proName;
  }


  public String getAnswer() {
    return answer;
  }

  public void setAnswer(String answer) {
    this.answer = answer;
  }


  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }


  public String getShow() {
    return show;
  }

  public void setShow(String show) {
    this.show = show;
  }


  public String getTextType() {
    return textType;
  }

  public void setTextType(String textType) {
    this.textType = textType;
  }

}

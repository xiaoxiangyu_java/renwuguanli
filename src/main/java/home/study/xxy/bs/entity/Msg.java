package home.study.xxy.bs.entity;


import java.io.Serializable;

public class Msg  implements Serializable {


    private String msg;

    private Integer id;

    private String userNick;
    private String userId;

    private String user1Nick;
    private String user1Id;


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserNick() {
        return userNick;
    }

    public void setUserNick(String userNick) {
        this.userNick = userNick;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUser1Nick() {
        return user1Nick;
    }

    public void setUser1Nick(String user1Nick) {
        this.user1Nick = user1Nick;
    }

    public String getUser1Id() {
        return user1Id;
    }

    public void setUser1Id(String user1Id) {
        this.user1Id = user1Id;
    }

    @Override
    public String toString() {
        return "Msg{" +
                "msg='" + msg + '\'' +
                ", id=" + id +
                ", userNick='" + userNick + '\'' +
                ", userId='" + userId + '\'' +
                ", user1Nick='" + user1Nick + '\'' +
                ", user1Id='" + user1Id + '\'' +
                '}';
    }
}

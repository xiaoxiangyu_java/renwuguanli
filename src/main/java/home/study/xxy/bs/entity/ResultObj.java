package home.study.xxy.bs.entity;

import java.util.HashMap;
import java.util.List;

/**
 * @author xiaoxiangyu
 * @create 2022-05-17 18:58
 * @功能：返回信息  是否执行成功 执行状态码 返回消息 返回数据
 */
public class ResultObj {

//    public static int

    /**
     * 是否成功
     */
    private Boolean success;

    /**
     * 执行状态码
     */
    private String code;

    /**
     * 返回消息
     */
    private String message;


    /**
     * 数据
     */
    private Object data;


    public ResultObj() {
    }

    public ResultObj(Boolean success,CodeEnum e) {
        this.success = success;
        this.code = e.code;
        this.message = e.message;
    }

    public ResultObj(Boolean success, String code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }

    public ResultObj(Boolean success, String code, String message, Object data) {
        this.success = success;
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /**
     * 登录成功结果集
     * @return
     */
    public static ResultObj logingOk(){
        ResultObj result = new ResultObj();
        result.setSuccess(true);
        result.setCode(CodeEnum.REQUEST_CODE_LOGIN_SUCCESS.getCode());
        result.setMessage(CodeEnum.REQUEST_CODE_LOGIN_SUCCESS.getMessage());
        return result;
    }

    /**
     * 登录失败结果集
     * @return
     */
    public static ResultObj logingFail(){
        ResultObj result = new ResultObj();
        result.setSuccess(true);
        result.setCode(CodeEnum.REQUEST_CODE_LOGIN_SUCCESS.getCode());
        result.setMessage(CodeEnum.REQUEST_CODE_LOGIN_SUCCESS.getMessage());
        return result;
    }

    /**
     * 成功结果集
     * @return
     */
    public static ResultObj ok(){
        ResultObj result = new ResultObj();
        result.setSuccess(true);
        result.setCode(CodeEnum.REQUEST_CODE_SUCCESS.getCode());
        result.setMessage(CodeEnum.REQUEST_CODE_SUCCESS.getMessage());
        return result;
    }

    /**
     * 成功结果集
     * @return
     */
    public static ResultObj ok(CodeEnum success){
        ResultObj result = new ResultObj();
        result.setSuccess(true);
        result.setCode(success.getCode());
        result.setMessage(success.getMessage());
        return result;
    }


    /**
     * 权限不足结果集
     * @return
     */
    public static ResultObj accFail(){
        ResultObj result = new ResultObj();
        result.setSuccess(true);
        result.setCode(CodeEnum.REQUEST_CODE_ACCESS_FAIL.getCode());
        result.setMessage(CodeEnum.REQUEST_CODE_ACCESS_FAIL.getMessage());
        return result;
    }

    /**
     * 验证错误结果集
     * @return
     */
    public static ResultObj AuthenFail(){
        ResultObj result = new ResultObj();
        result.setSuccess(false);
        result.setCode(CodeEnum.REQUEST_CODE_AUTHEN_FAIL.getCode());
        result.setMessage(CodeEnum.REQUEST_CODE_AUTHEN_FAIL.getMessage());
        return result;
    }

    /**
     * 失败结果集
     * @return
     */
    public static ResultObj error(){
        ResultObj result = new ResultObj();
        result.setSuccess(false);
        result.setCode(CodeEnum.REQUEST_CODE_FAIL.getCode());
        result.setMessage(CodeEnum.REQUEST_CODE_FAIL.getMessage());
        return result;
    }

    /**
     * 错误结果集
     * @param codeEnum
     * @return
     */
    public static ResultObj fail(CodeEnum codeEnum){
        ResultObj result = new ResultObj();
        result.setSuccess(false);
        result.setCode(codeEnum.getCode());
        result.message(codeEnum.getMessage());
        return result;
    }


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public ResultObj message(String message) {
        this.message = message;
        return this;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}

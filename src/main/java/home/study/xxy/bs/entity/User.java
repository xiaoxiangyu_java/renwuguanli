package home.study.xxy.bs.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import home.study.xxy.bs.utils.DateUtils;
import home.study.xxy.bs.utils.ExcelImport;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;
import java.io.Serializable;
import java.util.Date;

// implements UserDetails
public class User implements Serializable{

    /**行号*/
    @TableField(exist = false)
    private int rowNum;

    /**错误提示*/
    @TableField(exist = false)
    private String rowTips;

    /**数据*/
    @TableField(exist = false)
    private String rowData;



  @ExcelImport(value = "id")
  @ApiModelProperty(value = "id", dataType = "String")
  private String id;
  @ExcelImport(value = "userName",maxLength = 11)
  @ApiModelProperty(value = "用户名", dataType = "String")
  private String userName;
  @ExcelImport(value = "pwd")
  @ApiModelProperty(value = "密码", dataType = "String")
  private String pwd;
  @ExcelImport(value = "uNick")
  @ApiModelProperty(value = "昵称", dataType = "String")
  private String uNick;
  @ExcelImport(value = "uAdmin")
  @ApiModelProperty(value = "是否为管理员", dataType = "String")
  private String uAdmin;
  @ExcelImport(value = "userInt",required = true)
  @ApiModelProperty(value = "个性签名", dataType = "String")
  private String userInt;
  @DateTimeFormat(pattern = DateUtils.YYYY_MM_DD)
  @ApiModelProperty(value = "会员时间", dataType = "String")
  @ExcelImport(value = "vipTime")
  private Date vipTime;



  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }


  public String getPwd() {
    return pwd;
  }

  public void setPwd(String pwd) {
    this.pwd = pwd;
  }


  public String getUNick() {
    return uNick;
  }

  public void setUNick(String uNick) {
    this.uNick = uNick;
  }


  public String getUAdmin() {
    return uAdmin;
  }

  public void setUAdmin(String uAdmin) {
    this.uAdmin = uAdmin;
  }


  public String getUserInt() {
    return userInt;
  }

  public void setUserInt(String userInt) {
    this.userInt = userInt;
  }


  public Date getVipTime() {
    return vipTime;
  }

  public void setVipTime(Date vipTime) {
    this.vipTime = vipTime;
  }

  @Override
  public String toString() {
    return "User{" +
            "id='" + id + '\'' +
            ", userName='" + userName + '\'' +
            ", pwd='" + pwd + '\'' +
            ", uNick='" + uNick + '\'' +
            ", uAdmin='" + uAdmin + '\'' +
            ", userInt='" + userInt + '\'' +
            ", vipTime=" + vipTime +
            '}';
  }
}

package home.study.xxy.bs.entity;


import java.io.Serializable;

/**
 * 类描述：
 *  实体类
 *
 * @author xiaoxiangyu
 * @date 2022-05-17 16:14
 */
public class UserRole implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer mapId;

    private Integer userId;

    private Integer roleId;

    private String roleName;
    public static final String MAP_ID = "map_id";
    public static final String USER_ID = "user_id";
    public static final String ROLE_ID = "role_id";


    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getMap_id() {
        return mapId;
    }

    public void setMap_id(Integer map_id) {
        this.mapId = map_id;
    }

    public Integer getUser_id() {
        return userId;
    }

    public void setUser_id(Integer user_id) {
        this.userId = user_id;
    }

    public Integer getRole_id() {
        return roleId;
    }

    public void setRole_id(Integer role_id) {
        this.roleId = role_id;
    }

    public static String getMapId() {
        return MAP_ID;
    }

    public static String getUserId() {
        return USER_ID;
    }

    public static String getRoleId() {
        return ROLE_ID;
    }
}

package home.study.xxy.bs.entity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * @author xiaoxiangyu
 * @create 2022-05-25 20:37
 * @功能：
 */
@Data
public class LoginUser implements UserDetails {

    private User user;

    private String token;

    private List<String> permissions;

    @JSONField(serialize = false)
    private HashSet<GrantedAuthority> perSet;

    public LoginUser() {
    }

    public LoginUser(User user, List<String> permissions) {
        this.user = user;
        this.permissions = permissions;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        perSet = new HashSet<>();

        for (String permission : permissions) {
            perSet.add(new SimpleGrantedAuthority(permission));
        }
        
        
        return perSet;
    }

    @Override
    public String getPassword() {
        return user.getPwd();
    }

    @Override
    public String getUsername() {
        return user.getUserName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}

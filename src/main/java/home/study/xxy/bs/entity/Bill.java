package home.study.xxy.bs.entity;


import java.io.Serializable;
import java.util.Date;

public class Bill implements Serializable {

  private String billId;
  private Date createTime;
  private String gold;
  private String billState;
  private String userId;


  public String getBillId() {
    return billId;
  }

  public void setBillId(String billId) {
    this.billId = billId;
  }


  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }


  public String getGold() {
    return gold;
  }

  public void setGold(String gold) {
    this.gold = gold;
  }


  public String getBillState() {
    return billState;
  }

  public void setBillState(String billState) {
    this.billState = billState;
  }


  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

}

package home.study.xxy.bs.entity;


import java.util.Date;

public class CountDay {

  private String countId;
  private String count;
  private Date date;


  public String getCountId() {
    return countId;
  }

  public void setCountId(String countId) {
    this.countId = countId;
  }


  public String getCount() {
    return count;
  }

  public void setCount(String count) {
    this.count = count;
  }


  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

}

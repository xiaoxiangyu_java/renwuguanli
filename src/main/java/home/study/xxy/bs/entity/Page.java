package home.study.xxy.bs.entity;

import java.io.Serializable;

/**
 * @title: Page
 * @功能: 分页信息
 * @Author Xiao Xiangyu
 * @Date: 2021/4/18 16:15
 * @Version 1.0
 */
public class Page implements Serializable {

    private Integer pageNumber;//当前第几页
    private Integer sumPage;//总页码
    private Integer total;//记录总数
    private Integer pageSize = 5;//每页记录数
    private Integer index;//分页查询的起始下标

    private boolean hasNext;//是否能下一页
    private boolean hasPre;//是否能上一页

    public Integer getpageNumber() {
        return pageNumber;
    }

    public void setpageNumber(Integer pageNumber) {
        pageNumber = pageNumber > 0 ? pageNumber : 1;
        this.pageNumber = pageNumber;
    }

    public Integer getSumPage() {
        if (total / pageSize == 0) {
            sumPage = total / pageSize;
        } else {
            sumPage = total / pageSize + 1;
        }
        return sumPage;
    }

    public void setSumPage(Integer sumPage) {
        this.sumPage = sumPage;
    }

    public Integer gettotal() {
        return total;
    }

    public void settotal(Integer total) {
        this.total = total;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getIndex() {
        index = (getpageNumber() - 1) * getPageSize();
        return index;
    }

    public boolean isHasNext() {
        return getpageNumber() < getSumPage();
    }

    public boolean isHasPre() {
        return getpageNumber() > 1;
    }

    @Override
    public String toString() {
        return "page [pageNumber=" + pageNumber + ", sumPage=" + sumPage + ", total=" + total + ", pageSize=" + pageSize
                + ", index=" + index + ", hasNext=" + hasNext + ", hasPre=" + hasPre + "]";
    }

    public Page() {
        pageNumber = 1;

    }

    public Page(Integer pageNumber, Integer sumPage, Integer total, Integer pageSize, Integer index, boolean hasNext,
                boolean hasPre) {
        super();
        this.pageNumber = pageNumber;
        this.sumPage = sumPage;
        this.total = total;
        this.pageSize = pageSize;
        this.index = index;
        this.hasNext = hasNext;
        this.hasPre = hasPre;
    }

}

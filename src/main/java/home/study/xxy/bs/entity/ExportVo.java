package home.study.xxy.bs.entity;

import com.baomidou.mybatisplus.annotation.TableField;

public class ExportVo {

    /**行号*/
    @TableField(exist = false)
    private int rowNum;

    /**错误提示*/
    @TableField(exist = false)
    private String rowTips;

    /**数据*/
    @TableField(exist = false)
    private String rowData;


    public int getRowNum() {
        return rowNum;
    }

    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }

    public String getRowTips() {
        return rowTips;
    }

    public void setRowTips(String rowTips) {
        this.rowTips = rowTips;
    }

    public String getRowData() {
        return rowData;
    }

    public void setRowData(String rowData) {
        this.rowData = rowData;
    }
}

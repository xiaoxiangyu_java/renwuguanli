package home.study.xxy.bs.entity;


import java.io.Serializable;
import java.util.Date;

public class Order implements Serializable {

  private String orderId;
  private String jobId;
  private String userId;
  private String jobState;
  private Date createTime;
  private Date finishTime;
  private String jobStaff;
  private Date deleteTime;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }


  public String getJobId() {
    return jobId;
  }

  public void setJobId(String jobId) {
    this.jobId = jobId;
  }


  public String getjobState() {
    return jobState;
  }

  public void setjobState(String jobState) {
    this.jobState = jobState;
  }


  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }


  public Date getFinishTime() {
    return finishTime;
  }

  public void setFinishTime(Date finishTime) {
    this.finishTime = finishTime;
  }


  public String getJobStaff() {
    return jobStaff;
  }

  public void setJobStaff(String jobStaff) {
    this.jobStaff = jobStaff;
  }


  public Date getDeleteTime() {
    return deleteTime;
  }

  public void setDeleteTime(Date deleteTime) {
    this.deleteTime = deleteTime;
  }

}

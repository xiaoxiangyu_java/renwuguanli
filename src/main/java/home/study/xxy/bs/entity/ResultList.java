package home.study.xxy.bs.entity;

import java.util.List;

/**
 * @title: ResultList
 * @功能: 返回的结果集
 * @Author Xiao Xiangyu
 * @Date: 2021/4/18 18:32
 * @Version 1.0
 */
public class ResultList<T> {
    private Object list;
    private int total;

    public Object getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}

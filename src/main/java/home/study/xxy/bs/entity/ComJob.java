package home.study.xxy.bs.entity;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class ComJob implements Serializable {

  /**
   * 任务id
   */
  @TableId(type = IdType.AUTO)
  private String jobId;
  /**
   * 任务名称
   */
  private String jobName;
  /**
   * 任务介绍
   */
  private String jobDescribe;
  /**
   * 任务限定时间
   */
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date jobTime;
  /**
   * 接受 员工id
   */
  private String staffId;
  /**
   * 金额
   */
  private String gold;
  /**
   * 发布任务 人
   */
  private String userId;
  /**
   * 任务状态  0 审核中 1 未接受 2 已被接受 3已完成 -- 4审核未通过
   */
  private String jobState;
  /**
   * 任务类型
   */
  private String jobType;
  /**
   * 任务审核者
   */
  private String jobExamine;
  /**
   * 创建时间
   */
  @TableField(fill = FieldFill.INSERT)
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date createTime;
  /**
   * 完成时间
   */
  private Date finishTime;
  /**
   * 修改时间
   */
  @TableField(fill = FieldFill.INSERT_UPDATE)
  private Date updateTime;

  /**
   * 任务定级
   */
  private String jobLv;

  public Date getFinishTime() {
    return finishTime;
  }

  public void setFinishTime(Date finishTime) {
    this.finishTime = finishTime;
  }

  public String getJobExamine() {
    return jobExamine;
  }

  public void setJobExamine(String jobExamine) {
    this.jobExamine = jobExamine;
  }

  public String getJobType() {
    return jobType;
  }

  public void setJobType(String jobType) {
    this.jobType = jobType;
  }

  public String getJobId() {
    return jobId;
  }

  public void setJobId(String jobId) {
    this.jobId = jobId;
  }


  public String getJobName() {
    return jobName;
  }

  public void setJobName(String jobName) {
    this.jobName = jobName;
  }


  public String getJobDescribe() {
    return jobDescribe;
  }

  public void setJobDescribe(String jobDescribe) {
    this.jobDescribe = jobDescribe;
  }


  public Date getJobTime() {
    return jobTime;
  }

  public void setJobTime(Date jobTime) {
    this.jobTime = jobTime;
  }

  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

  public String getStaffId() {
    return staffId;
  }

  public void setStaffId(String staffId) {
    this.staffId = staffId;
  }


  public String getGold() {
    return gold;
  }

  public void setGold(String gold) {
    this.gold = gold;
  }


  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }


  public String getJobState() {
    return jobState;
  }

  public void setJobState(String jobState) {
    this.jobState = jobState;
  }


  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public String getJobLv() {
    return jobLv;
  }

  public void setJobLv(String jobLv) {
    this.jobLv = jobLv;
  }
}

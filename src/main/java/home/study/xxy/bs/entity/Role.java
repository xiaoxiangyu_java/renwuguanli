package home.study.xxy.bs.entity;



import java.io.Serializable;

/**
 * 类描述：
 *  实体类
 *
 * @author xiaoxiangyu
 * @date 2022-05-17 15:57
 */

public class Role implements Serializable {

    private static final long serialVersionUID = 1L;

    private String roleId;

    private String role;

    private String roleName;

    public static final String ROLE_ID = "role_id";
    public static final String ROLE = "role";
    public static final String ROLE_NAME = "role_name";


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getRole_id() {
        return roleId;
    }

    public void setRole_id(String role_id) {
        this.roleId = role_id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole_name() {
        return roleName;
    }

    public void setRole_name(String role_name) {
        this.roleName = role_name;
    }

    public static String getRoleId() {
        return ROLE_ID;
    }

    public static String getROLE() {
        return ROLE;
    }

    public static String getRoleName() {
        return ROLE_NAME;
    }
}

package home.study.xxy.bs.entity;


import java.io.Serializable;
import java.util.Date;

public class Chat implements Serializable {

  private String chatId;
  private String userO;
  private String userT;
  private Date createTime;
  private String message;


  public String getChatId() {
    return chatId;
  }

  public void setChatId(String chatId) {
    this.chatId = chatId;
  }


  public String getUserO() {
    return userO;
  }

  public void setUserO(String userO) {
    this.userO = userO;
  }


  public String getUserT() {
    return userT;
  }

  public void setUserT(String userT) {
    this.userT = userT;
  }


  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}

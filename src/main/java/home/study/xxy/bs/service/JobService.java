package home.study.xxy.bs.service;

import home.study.xxy.bs.entity.ComJob;
import home.study.xxy.bs.entity.Page;
import home.study.xxy.bs.entity.ResultList;
import home.study.xxy.bs.entity.ResultObj;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface JobService {

    ResultObj queryAllJob(ComJob comJob,int index ,int size);


    ResultObj queryJobById(ComJob comJob, Page pageVo);

    ResultObj queryJobByStaffId(ComJob comJob, Page pageVo);

    ResultObj addJob(ComJob comJob);

    ResultObj updateJob(ComJob comJob);

    ResultObj changeJobState(ComJob comJob);

    ResultObj queryUserJobCount();

    int queryJobCountById(ComJob comJob);

    ResultObj deleteJob(String jobId);

    ResultObj shortTimeTaskAllocation();

    List<ComJob> findByIds(ArrayList<Integer> ids);
}

//package home.study.xxy.bs.service.impl;
//
//
//
//import home.study.xxy.bs.entity.ComJob;
//import home.study.xxy.bs.entity.Order;
//import home.study.xxy.bs.entity.Page;
//import home.study.xxy.bs.entity.ResultList;
//import home.study.xxy.bs.mapper.OrderMapper;
//import home.study.xxy.bs.service.OrderService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * @title: OrderServiceImpl
// * @功能:
// * @Author Xiao Xiangyu
// * @Date: 2021/4/19 21:46
// * @Version 1.0
// */
//@Service
//public class OrderServiceImpl implements OrderService {
//
//    @Autowired
//    private OrderMapper orderMapper;
//
//    /**
//     * 查询订单
//     * */
//    @Override
//    public ResultList<Order> queryAllOrder(Order order, Page pageVo) {
//        Map<String, Object> params = new HashMap<String, Object>();
//        params.put("page", pageVo.getIndex());
//        params.put("size", pageVo.getPageSize());
//        int total = orderMapper.queryOrderByIdCount(order);
//        List<Order> bList= orderMapper.queryAllOrder(order,params);
//        ResultList<Order> billList = new ResultList<Order>();
//        billList.setList(bList);
//        billList.setTotal(total);
//        return billList;
//    }
//}

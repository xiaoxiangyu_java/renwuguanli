package home.study.xxy.bs.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import home.study.xxy.bs.entity.CodeEnum;
import home.study.xxy.bs.entity.LoginUser;
import home.study.xxy.bs.entity.Page;
import home.study.xxy.bs.entity.ResultList;
import home.study.xxy.bs.entity.ResultObj;
import home.study.xxy.bs.entity.User;
import home.study.xxy.bs.mapper.LoginMapper;
import home.study.xxy.bs.service.LoginService;
import home.study.xxy.bs.utils.JWTUtil;
import home.study.xxy.bs.utils.MD5Utils;
import home.study.xxy.bs.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @title: LoginServiceImpl
 * @功能:
 * @Author Xiao Xiangyu
 * @Date: 2021/4/11 19:34
 * @Version 1.0
 */
@Service
public class LoginServiceImpl extends ServiceImpl<LoginMapper, User> implements LoginService {

    @Autowired
    private LoginMapper loginMapper;

    @Autowired
    private AuthenticationManager manager;


//    @Autowired
//    private RedisCache redisCache;



    public LoginServiceImpl() {
    }

    public int checkLogin(User user){
        String pwdMd5  = MD5Utils.stringToMD5(user.getPwd());
        return loginMapper.loginCheck(user.getUserName(),pwdMd5);
    }

    @Override
    public ResultObj getLoginData(User user) {
        //2022.05.27  添加spring'security 登录验证

        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user.getUserName(), user.getPwd());

        //使用spring security 认证
        Authentication authenticate = manager.authenticate(authentication);
        if (authenticate == null){
            ResultObj resultObj = new ResultObj();
            resultObj.setSuccess(true);
            resultObj.setCode(CodeEnum.REQUEST_CODE_LOGIN_FAIL.getCode());
            resultObj.setCode("400");

        }


        //生成token
        LoginUser authorities = (LoginUser) authenticate.getPrincipal();
        String id = authorities.getUser().getId();

        String token = JWTUtil.createToken(id);

        authorities.setToken(token);

//        HashMap<String, String> result = new HashMap<>();
//        String tokenUser = JWTUtil.createToken(token);
//        result.put("token", token);


        //保存用户信息到redis
//        redisCache.put("login:"+token,authorities);
        User user1 = authorities.getUser();
        user1.setPwd(null);
        authorities.setUser(user1);
        RedisUtils.getInstance().set("login:"+token,authorities);
        ResultObj resultObj = ResultObj.logingOk();
        resultObj.setData(authorities);

        return resultObj;

//        String pwdMd5  = MD5Utils.stringToMD5(user.getPwd());
//        return loginMapper.getLoginData(user.getUserName(),pwdMd5);
    }

    public ResultList<User> queryAll(User user, Page pageVo) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", pageVo.getIndex());
        params.put("size", pageVo.getPageSize());
        int total = loginMapper.queryUserCount();
        List<User> userList= loginMapper.queryAll(user, params);
        ResultList<User> userResultList = new ResultList<User>();
        userResultList.setList(userList);
        userResultList.setTotal(total);
        return userResultList;
    }

    @Override
    public List<User> findAll() {
        List<User> userList= loginMapper.selectList(null);
        return userList;
    }

    @Override
    public User findOne(String id) {
        User userList= loginMapper.selectOne(new QueryWrapper<User>().eq("id",id));
        return userList;
    }

    @Override
    public int addUser(User user) {
        String pwdMd5  = MD5Utils.stringToMD5(user.getPwd());
        user.setPwd(pwdMd5);
//        if(loginMapper.countUserName(user)!=1){
//            return false;
//        }
//        if(loginMapper.addUser(user)!=1){
//            return false;
//        }
        return loginMapper.addUser(user);
//        return true;
    }

    @Override
    public int changePwd(User user) {
        if (user.getPwd()!=null){
            String pwdMd5  = MD5Utils.stringToMD5(user.getPwd());
            user.setPwd(pwdMd5);
        }
        int result = loginMapper.changePwd(user);
        return result;
    }

    @Override
    public int updateAdmin(User user) {
        int result = loginMapper.updateAdmin(user);
        return result;
    }


}

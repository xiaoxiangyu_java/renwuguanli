package home.study.xxy.bs.service;

import home.study.xxy.bs.entity.*;

import java.util.Date;
import java.util.List;

public interface OtherService {

    /**
     * 查询流量
     * */
    ResultObj countSum();
    /**
     * 统计流量
     * */
    void addCount(Date date);

    /**
     * 添加问题公告
     * @param managementText
     * @return
     */
    ResultObj addPQ(ManagementText managementText);

    /**
     * 查询问题公告
     * @param pageVo
     * @return
     */
    ResultObj queryPQ(ManagementText managementText,Page pageVo);

    /**
     * 删除公告
     * @param id
     * @return
     */
    ResultObj deletePQ(String id);

    /**
     * 修改公告
     * @param managementText
     * @return
     */
    ResultObj updatePQ(ManagementText managementText);


    /**
     * 购买vip
     * @param user
     * @param month
     * @return
     */
    int addVip(User user,int month);
}

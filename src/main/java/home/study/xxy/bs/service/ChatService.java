package home.study.xxy.bs.service;

import home.study.xxy.bs.entity.Chat;

import java.util.List;

public interface ChatService {

    /**
     * 查询通知
     * @param userId
     * @return
     */
    List<Chat> queryChatById(String userId);
}

//package home.study.xxy.bs.service.impl;
//
//import home.study.xxy.bs.entity.User;
//import home.study.xxy.bs.mapper.LoginMapper;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//import java.util.Objects;
//
///**
// * @author xiaoxiangyu
// * @create 2022-05-17 16:36
// * @功能：
// */
//@Service
//public class UserDetailServiceImpl implements UserDetailsService {
//
//    //用户校验
//    @Autowired
//    private LoginMapper loginMapper;
////    LoginMapper apiService=SpringContextHolder.getBean(LoginMapper.class);
//
//
//    //权限
////    @Autowired
////    private UserRoleMapper userRoleMapper;
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//
//        //根据登录信息进行认证
//        //1、查询用户
//        //2、获取角色信息
//        User userData = loginMapper.getUserData(username);
////        User userData = new User();
////        userData.setUserName("test");
////        userData.setPwd("e10adc3949ba59abbe56e057f20f883e");
////        userData.setId("2");
//
//
//        if (Objects.isNull(userData)) {
//            throw new RuntimeException("用户对象为空");
//        }
//
//        //2、
////        UserRole userRole = userRoleMapper.queryUserRole(userData.getId());
////        userData.setStaus(true);
////        userData.setPasswordNonExpired(true);
//
//        return userData;
//
//    }
//
//}

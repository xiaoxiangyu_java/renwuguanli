package home.study.xxy.bs.service.impl;

import home.study.xxy.bs.entity.LoginUser;
import home.study.xxy.bs.entity.User;
import home.study.xxy.bs.mapper.LoginMapper;
import home.study.xxy.bs.mapper.UserRoleMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xiaoxiangyu
 * @create 2022-05-25 20:29
 * @功能：
 */
@Service
@Slf4j
public class UserserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private LoginMapper loginMapper;

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        //查询用户信息
        User userData = loginMapper.getUserData(username);
        if (userData == null){
            log.error("验证对象为空!");
        }

        //查询权限信息
//        ArrayList<String> perList = new ArrayList<>();
        List<String> userRoles = userRoleMapper.queryUserRole(userData.getId());

        return new LoginUser(userData, userRoles);
    }
}

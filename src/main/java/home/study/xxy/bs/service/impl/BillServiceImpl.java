package home.study.xxy.bs.service.impl;

import home.study.xxy.bs.entity.*;
import home.study.xxy.bs.mapper.BillMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @title: BillServiceImpl
 * @功能:
 * @Author Xiao Xiangyu
 * @Date: 2021/4/23 21:29
 * @Version 1.0
 */
@Service
public class BillServiceImpl implements home.study.xxy.bs.service.BillService {
    @Autowired
    private BillMapper billMapper;

    /**
     * 查询订单
     * */
    @Override
    public ResultObj queryAllBill(Bill bill, Page pageVo) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", pageVo.getIndex());
        params.put("size", pageVo.getPageSize());
        int total = billMapper.queryBillByIdCount(bill);
        List<Bill> orders = billMapper.queryAllBill(bill, params);
        ResultList<Bill> jobList = new ResultList<Bill>();
        jobList.setList(orders);
        jobList.setTotal(total);
        if (orders != null && orders.size()>0){
            return new ResultObj(true,CodeEnum.REQUEST_CODE_QUERY_SUCCESS.getCode(),CodeEnum.REQUEST_CODE_QUERY_SUCCESS.getMessage(),jobList);
        }else{
            return new ResultObj(true,CodeEnum.REQUEST_CODE_QUERY_FAIL.getCode(),CodeEnum.REQUEST_CODE_QUERY_FAIL.getMessage());
        }
    }

}

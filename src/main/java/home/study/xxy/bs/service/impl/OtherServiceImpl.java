package home.study.xxy.bs.service.impl;

import home.study.xxy.bs.entity.*;
import home.study.xxy.bs.mapper.LoginMapper;
import home.study.xxy.bs.mapper.OtherMapper;
import home.study.xxy.bs.service.LoginService;
import home.study.xxy.bs.service.OtherService;
import home.study.xxy.bs.utils.MD5Utils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @title: LoginServiceImpl
 * @功能:
 * @Author Xiao Xiangyu
 * @Date: 2021/4/11 19:34
 * @Version 1.0
 */
@Service
@Slf4j
public class OtherServiceImpl implements OtherService {

    @Autowired
    private OtherMapper otherMapper;

    @Override
    public ResultObj countSum() {
        List<CountDay> countDays = otherMapper.countSum();
        if (countDays != null && countDays.size()>0){
            return new ResultObj(true,CodeEnum.REQUEST_CODE_QUERY_SUCCESS.getCode(),CodeEnum.REQUEST_CODE_QUERY_SUCCESS.getMessage(),countDays);
        }else{
            return new ResultObj(true,CodeEnum.REQUEST_CODE_QUERY_FAIL.getCode(),CodeEnum.REQUEST_CODE_QUERY_FAIL.getMessage());
        }

    }

    @Override
    public void addCount(Date date) {
        otherMapper.addCount(date);
    }

    @Override
    public ResultObj addPQ(ManagementText managementText) {
        if (managementText.getTextType()!=null && managementText.getAnswer()!=null&&managementText.getShow()!=null){
            managementText.setCreateTime(new Date());
            int contS =  otherMapper.addPQ(managementText);
            if (contS == 1){
                log.info("公告添加成功");
                return new ResultObj(true,CodeEnum.REQUEST_CODE_ADD_SUCCESS.getCode(),CodeEnum.REQUEST_CODE_ADD_SUCCESS.getMessage());
            }else {
                log.error("公告添加错误");
                return new ResultObj(true,CodeEnum.REQUEST_CODE_ADD_FAIL.getCode(),CodeEnum.REQUEST_CODE_ADD_FAIL.getMessage());
            }
        }else{
            log.error("公告问题信息为空");
            return new ResultObj(true,CodeEnum.REQUEST_CODE_ADD_FAIL.getCode(),CodeEnum.REQUEST_CODE_ADD_FAIL.getMessage());
        }

    }

    @Override
    public ResultObj queryPQ(ManagementText managementText, Page pageVo) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", pageVo.getIndex());
        params.put("size", pageVo.getPageSize());
        int total = otherMapper.queryTextCount(managementText);
        List<ManagementText> comJobList= otherMapper.queryPQ(managementText,params);;
        ResultList<ManagementText> resultList = new ResultList<ManagementText>();
        resultList.setList(comJobList);
        resultList.setTotal(total);
        if (comJobList != null && comJobList.size()>0){
            return new ResultObj(true,CodeEnum.REQUEST_CODE_QUERY_SUCCESS.getCode(),CodeEnum.REQUEST_CODE_QUERY_SUCCESS.getMessage(),resultList);
        }else{
            return new ResultObj(true,CodeEnum.REQUEST_CODE_QUERY_FAIL.getCode(),CodeEnum.REQUEST_CODE_QUERY_FAIL.getMessage());
        }

    }

    @Override
    public ResultObj deletePQ(String id) {
      if (otherMapper.deletePQ(id)==1){
          return new ResultObj(true,CodeEnum.REQUEST_CODE_DELETE_SUCCESS.getCode(),CodeEnum.REQUEST_CODE_DELETE_SUCCESS.getMessage());
      }else{
          return new ResultObj(true,CodeEnum.REQUEST_CODE_DELETE_FAIL.getCode(),CodeEnum.REQUEST_CODE_DELETE_FAIL.getMessage());
      }
    }

    @Override
    public ResultObj updatePQ(@Param("text") ManagementText managementText) {
        if (otherMapper.updatePQ(managementText)==1){
            return new ResultObj(true,CodeEnum.REQUEST_CODE_UPDATE_SUCCESS.getCode(),CodeEnum.REQUEST_CODE_UPDATE_SUCCESS.getMessage());
        }else{
            return new ResultObj(true,CodeEnum.REQUEST_CODE_UPDATE_FAIL.getCode(),CodeEnum.REQUEST_CODE_UPDATE_FAIL.getMessage());
        }
    }


    @Override
    public int addVip(User user, int month) {
        Date date = otherMapper.queryVip(user, month);
        user.setVipTime(date);
        return otherMapper.addVip(user);
    }
}

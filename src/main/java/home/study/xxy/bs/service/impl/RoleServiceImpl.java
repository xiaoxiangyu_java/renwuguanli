package home.study.xxy.bs.service.impl;

import home.study.xxy.bs.entity.Role;
import home.study.xxy.bs.mapper.RoleMapper;
import home.study.xxy.bs.service.RoleService;
import org.springframework.stereotype.Service;

/**
 * 类描述：
 *   服务实现类
 *
 * @author chenjs
 * @date 2022-05-17 15:57
 */
@Service
public class RoleServiceImpl implements RoleService {

}

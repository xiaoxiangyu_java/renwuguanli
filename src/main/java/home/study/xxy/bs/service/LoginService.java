package home.study.xxy.bs.service;

import java.util.List;

import home.study.xxy.bs.entity.Page;
import home.study.xxy.bs.entity.ResultList;
import home.study.xxy.bs.entity.ResultObj;
import home.study.xxy.bs.entity.User;

public interface LoginService {

    /**
     * 登录验证
     */
    int checkLogin(User user);

    /**
     * 登录通过后存储用户信息
     */
    ResultObj getLoginData(User user);

    /**
     * 查询所有用户
     */
    ResultList<User> queryAll(User user, Page pageVo);

    /**
     * 查询所有用户
     */
    List<User> findAll();

    /**
     * 查询用户
     */
    User findOne(String id);

    /**
     * 注册用户
     *
     * @return*/
    int addUser(User user);

    /**
     * 修改用户信息
     * */
    int changePwd(User user);
    /**
     * 修改权限信息
     * */
    int updateAdmin(User user);
}

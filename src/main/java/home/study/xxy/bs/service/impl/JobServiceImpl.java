package home.study.xxy.bs.service.impl;

import javax.annotation.Resource;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import home.study.xxy.bs.assignment.Assignment;
import home.study.xxy.bs.assignment.ShortTimeAss;
import home.study.xxy.bs.entity.*;
import home.study.xxy.bs.mapper.BillMapper;
import home.study.xxy.bs.mapper.JobMapper;
import home.study.xxy.bs.mapper.UserRoleMapper;
import home.study.xxy.bs.service.JobService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @title: JobServiceImpl
 * @功能: 任务管理实现层
 * @Author Xiao Xiangyu
 * @Date: 2021/4/18 15:39
 * @Version 1.0
 */
@Slf4j
@Service
public class JobServiceImpl implements JobService {

    @Resource
    private UserRoleMapper userRoleMapper;

    @Resource
    private JobMapper jobMapper;
    @Resource
    private BillMapper billMapper;

    @Autowired
    private ShortTimeAss assignment;

    /**
     * 查询所有任务
     * */
    @Override
    public ResultObj queryAllJob(ComJob comJob, int index ,int size) {
        Map<String, Object> params = new HashMap<String, Object>();
//        params.put("page", pageVo.getIndex());
//        params.put("size", pageVo.getPageSize());
       com.baomidou.mybatisplus.extension.plugins.pagination.Page<ComJob> page = new com.baomidou.mybatisplus.extension.plugins.pagination.Page(index,size);
//        int total = jobMapper.queryJobCount(comJob);
        QueryWrapper<ComJob> queryWrapper = new QueryWrapper<>();
        IPage<ComJob> iPage = jobMapper.selectPage(page, queryWrapper);
//        List<ComJob> comJobList= jobMapper.queryAll(comJob,params);
        List<ComJob> comJobList= iPage.getRecords();
        ResultList<ComJob> jobList = new ResultList<ComJob>();
        jobList.setList(comJobList);
        jobList.setTotal((int) page.getTotal());
        if (comJobList != null && comJobList.size() > 0){
            return new ResultObj(true,CodeEnum.REQUEST_CODE_QUERY_SUCCESS.getMessage(),
                    CodeEnum.REQUEST_CODE_QUERY_SUCCESS.getCode(),jobList);
        }else{
            return new ResultObj(true,CodeEnum.REQUEST_CODE_QUERY_FAIL.getMessage(),
                    CodeEnum.REQUEST_CODE_QUERY_FAIL.getCode());
        }

    }
    /**
     * 查询每个用户接的任务
     * */
    @Override
    public ResultObj queryUserJobCount() {
        List list = jobMapper.queryUserJobCount();
        if (list != null && list.size() > 0){
            return new ResultObj(true,CodeEnum.REQUEST_CODE_QUERY_SUCCESS.getCode(),
                    CodeEnum.REQUEST_CODE_QUERY_SUCCESS.getMessage(),list);
        }else{
            return new ResultObj(true,CodeEnum.REQUEST_CODE_QUERY_FAIL.getCode(),
                    CodeEnum.REQUEST_CODE_QUERY_FAIL.getMessage());
        }

    }
    /**
     * 根据id查询用户接的任务
     * */
    @Override
    public int queryJobCountById(ComJob comJob) {
        int i = jobMapper.queryJobCountById(comJob);
        return i;
    }

    @Override
    public List<ComJob> findByIds(ArrayList<Integer> ids) {

        List<ComJob> comJobs = jobMapper.selectBatchIds(ids);

        return comJobs;
    }

    /**
     * 通过任务id删除任务
     * @param jobId
     * @return
     */
    @Override
    public ResultObj deleteJob(String jobId) {
        int i = jobMapper.deleteJob(jobId);
        if (i == 1){
            return new ResultObj(true,CodeEnum.REQUEST_CODE_DELETE_SUCCESS.getCode(),CodeEnum.REQUEST_CODE_DELETE_SUCCESS.getMessage());
        }else{
            return new ResultObj(true,CodeEnum.REQUEST_CODE_DELETE_FAIL.getCode(),CodeEnum.REQUEST_CODE_DELETE_FAIL.getMessage());
        }
    }

    /**
     * 修改任务状态
     * */
    @Override
    public ResultObj changeJobState(ComJob comJob) {
        int updateJobState = jobMapper.changeJobState(comJob);
        if ("3".equals(comJob.getJobState())){
            billMapper.changeBill(comJob);
        }

        //开启定时分配  1、使用延时队列
        // 结合Java spi (好像不需要。。)
        ServiceLoader<Assignment> load = ServiceLoader.load(Assignment.class);

        new Thread(()-> {
            assignment.getDelayQueue(10 * 1000,comJob);
//            for (Assignment assignment : load) {
//                //通过反射的获取的对象
//                Assignment bean = SpringContextUtils.getBean(assignment.getClass());
//                bean.getDelayQueue(10 * 1000,comJob);
////                2 * 60 * 6
//            }

        }).start();

        if (updateJobState != 1){
            return new ResultObj(true,CodeEnum.REQUEST_CODE_UPDATE_SUCCESS.getMessage(),
                    CodeEnum.REQUEST_CODE_UPDATE_SUCCESS.getCode());
        }else{
            return new ResultObj(true,CodeEnum.REQUEST_CODE_UPDATE_FAIL.getMessage(),
                    CodeEnum.REQUEST_CODE_UPDATE_FAIL.getCode());
        }

        //开启定时分配  1、使用延时队列
//        System.out.println(DateFormat.getDateTimeInstance().format(new Date()));
//        DelayQueue<ShortTimeAss> shortTimeAsses = new DelayQueue<ShortTimeAss>();
//        shortTimeAss.setDelayTime(2000);
//        shortTimeAsses.put(shortTimeAss);
//        try {
//            shortTimeAsses.take().assJobs();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }


        //使用线程池
//        shortTimeAss.assJobs();
//        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(2, 100, 500, TimeUnit.SECONDS, new LinkedBlockingDeque<>());
//        threadPoolExecutor.execute(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    Thread.sleep(10000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                shortTimeAss.assJobs();
//            }
//        });



    }

//    /**
//     * 根据用户id查询所有任务
//     *
//     * @return 查询结果和总数
//     * */
//    @Override
//    public ResultList<ComJob> queryJobById(ComJob comJob, Page pageVo, Date startTime, Date endTime) {
//        Map<String, Object> params = new HashMap<String, Object>();
//        params.put("page", pageVo.getIndex());
//        params.put("size", pageVo.getPageSize());
//        int total = jobMapper.queryJobByIdCount(comJob,startTime,endTime);
//        List<ComJob> comJobList= jobMapper.queryJobById(comJob,params,startTime,endTime);
//        ResultList<ComJob> jobList = new ResultList<ComJob>();
//        jobList.setList(comJobList);
//        jobList.setTotal(total);
//        return jobList;
//    }
    @Override
    public ResultObj queryJobById(ComJob comJob, Page pageVo) {


//        new LambdaQueryChainWrapper<>(jobMapper)
//                .eq(ComJob::getJobId, "1")
//                .eq(ComJob::getJobName, "test").one();


        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", pageVo.getIndex());
        params.put("size", pageVo.getPageSize());
        int total = jobMapper.queryJobByIdCount(comJob);
        List<ComJob> comJobList= jobMapper.queryJobById(comJob,params);
        if (comJobList != null && comJobList.size() > 0){
            for (int i = 0; i < comJobList.size(); i++) {
                comJobList.get(i).setJobId( "XXY"+(Integer.valueOf(comJobList.get(i).getJobId())+9635412));
            }
            ResultList<ComJob> jobList = new ResultList<ComJob>();
            jobList.setList(comJobList);
            jobList.setTotal(total);
            return new ResultObj(true,CodeEnum.REQUEST_CODE_QUERY_SUCCESS.getCode(),
                    CodeEnum.REQUEST_CODE_QUERY_SUCCESS.getMessage(),jobList);
        }else{
            return new ResultObj(true,CodeEnum.REQUEST_CODE_QUERY_FAIL.getCode(),
                    CodeEnum.REQUEST_CODE_QUERY_FAIL.getMessage());
        }
    }
    /**
     * 根据接任务id查询所有任务
     *
     * @return 查询结果和总数
     * */
    @Override
    public ResultObj queryJobByStaffId(ComJob comJob,Page pageVo) {
        Map<String, Object> params = new HashMap<>();
        params.put("page", pageVo.getIndex());
        params.put("size", pageVo.getPageSize());
        int total = jobMapper.queryJobByIdCount(comJob);
        List<ComJob> comJobList= jobMapper.queryJobByStaffId(comJob,params);
        ResultList<ComJob> jobList = new ResultList<ComJob>();
        jobList.setList(comJobList);
        jobList.setTotal(total);
        if (comJobList != null && comJobList.size() > 0){
            return new ResultObj(true,CodeEnum.REQUEST_CODE_QUERY_SUCCESS.getCode(),
                    CodeEnum.REQUEST_CODE_QUERY_SUCCESS.getMessage(),jobList);
        }else{
            return new ResultObj(true,CodeEnum.REQUEST_CODE_QUERY_FAIL.getCode(),
                    CodeEnum.REQUEST_CODE_QUERY_FAIL.getMessage());
        }
    }

    /**
     * 添加任务
     * 成功时添加账单
     * */
    @Override
    public ResultObj updateJob(ComJob comJob) {

        jobMapper.updateById(comJob);

        return new ResultObj(true,CodeEnum.REQUEST_CODE_UPDATE_SUCCESS.getCode(),CodeEnum.REQUEST_CODE_UPDATE_SUCCESS.getMessage());
    }
    /**
     * 添加任务
     * 成功时添加账单
     * */
    @Override
    public ResultObj addJob(ComJob comJob) {
        //进行一些过滤  时间太短 价值太低  小于两天的
        long finishTime = comJob.getJobTime().getTime() - new Date().getTime();
        if (finishTime < 2 * 24 * 60 * 60 * 60 ){
            return new ResultObj(true,CodeEnum.REQUEST_CODE_ADD_FAIL.getCode(),CodeEnum.REQUEST_CODE_ADD_FAIL.getMessage());
        }
        //时薪低于两百的
        long hourGold = Integer.parseInt(comJob.getGold()) / (finishTime / 1000 / 60 / 60);
        if (hourGold < 200) {
            return new ResultObj(true,CodeEnum.REQUEST_CODE_ADD_FAIL.getCode(),CodeEnum.REQUEST_CODE_ADD_FAIL.getMessage());
        }

        //进行任务智能定级  定级 价值比在 200以上
        long lv = (hourGold - 200) / 500;
        comJob.setJobLv(lv+"");


        int i = jobMapper.insert(comJob);
//        int i = jobMapper.addJob(comJob);
        if (i==1){
            billMapper.addBill(comJob);
            return new ResultObj(true,CodeEnum.REQUEST_CODE_ADD_SUCCESS.getCode(),CodeEnum.REQUEST_CODE_ADD_SUCCESS.getMessage());
        }else{
            log.error("账单生成失败");
            return new ResultObj(true,CodeEnum.REQUEST_CODE_ADD_FAIL.getCode(),CodeEnum.REQUEST_CODE_ADD_FAIL.getMessage());
        }
    }


    /**
     * 最短时间优先 任务分配
     * @return
     */
    @Override
    public ResultObj shortTimeTaskAllocation() {

        //1、查未分配任务 和 全部员工 及 接受的任务
        List<Map> work = jobMapper.queryNoAllocationAll();
            List<Map> maps = jobMapper.queryWorkDate();

        System.out.println("test");
        //2、分配

        //最短完成时间优先 ---- > 等级高的员工优先接对应等级的，在此基础上分配下级任务，保证最短时间

        for (Map map : work) {
            map.get("job_lv");
        }









        return null;
    }
}

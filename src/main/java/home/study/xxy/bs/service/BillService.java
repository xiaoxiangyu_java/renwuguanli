package home.study.xxy.bs.service;

import home.study.xxy.bs.entity.Bill;
import home.study.xxy.bs.entity.Order;
import home.study.xxy.bs.entity.Page;
import home.study.xxy.bs.entity.ResultList;
import home.study.xxy.bs.entity.ResultObj;

import java.util.List;

public interface BillService {
    ResultObj queryAllBill(Bill bill, Page pageVo);
}

package home.study.xxy.bs.service.impl;

import home.study.xxy.bs.entity.Chat;
import home.study.xxy.bs.mapper.ChatMapper;
import home.study.xxy.bs.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @title: LoginServiceImpl
 * @功能:
 * @Author Xiao Xiangyu
 * @Date: 2021/4/11 19:34
 * @Version 1.0
 */
@Service
public class ChatServiceImpl implements ChatService {

    @Autowired
    private ChatMapper chatMapper;

    @Override
    public List<Chat> queryChatById(String userId){
        return chatMapper.queryChatById(userId);
    }
}

package home.study.xxy.bs.utils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

/**
 * @title: getToken
 * @功能:
 * @Author Xiao Xiangyu
 * @Date: 2021/4/12 21:43
 * @Version 1.0
 */
public class GetToken {
   public static String get(){
       String uuid = UUID.randomUUID().toString().replaceAll("-","");
       return uuid;
   }
}

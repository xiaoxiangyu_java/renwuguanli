package home.study.xxy.bs.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author xiaoxiangyu
 * @create 2022-05-28 23:24
 * @功能： 解决反射创建对象，放spring容器中
 */
@Component
public class SpringContextUtils implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringContextUtils.applicationContext = applicationContext;
    }

    public static <T> T getBean(Class<T> tClass){

        if (applicationContext == null){
            return null;
        }
        return applicationContext.getBean(tClass);


    }

    public static <T> T getBean(String beanName,Class<T> tClass){

        if (applicationContext == null){
            return null;
        }
        return applicationContext.getBean(beanName,tClass);


    }


}

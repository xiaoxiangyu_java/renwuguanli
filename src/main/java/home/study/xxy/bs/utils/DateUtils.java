package home.study.xxy.bs.utils;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

public class DateUtils {
	/**
	 * 日期格式：yyyy-MM-dd HH:mm:ss
	 */
	public static final String DATE_PATTERN_DEFAULT = "yyyy-MM-dd HH:mm:ss";
	/**
	 * 日期格式：yyyy-MM-dd HH:mm
	 */
	public static final String DATE_PATTERN_TIME_EN = "yyyy-MM-dd HH:mm";
	/**
	 * 日期格式：yyyy-MM-dd HH
	 */
	public static final String DATE_PATTERN_HOUR_EN = "yyyy-MM-dd HH";
	/**
	 * 日期格式：yyyy-MM-dd HH:mm
	 */
	public static final String DATE_PATTERN_TIME_CN = "yyyy年MM月dd日 HH:mm";
	/**
	 * 日期格式：yyyy-MM-dd hh:mm:ss.SSS
	 */
	public static final String YYYY_MM_DD_HH_MM_SS_SSS = "yyyy-MM-dd hh:mm:ss.SSS";
	/**
	 * 日期格式：yyyy-MM-dd
	 */
	public static final String YYYY_MM_DD = "yyyy-MM-dd";
	/**
	 * 日期格式：yyyy-MM
	 */
	public static final String YYYY_MM = "yyyy-MM";
	/**
	 * 日期格式： HH:mm:ss
	 */
	public static final String HH_MM_SS = "HH:mm:ss";
	/**
	 * 日期格式： HH:mm
	 */
	public static final String HH_MM= "HH:mm";
	/**
	 * 日期格式：yyyyMMddHHmmss
	 */
	public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
	/**
	 * 日期格式yyyyMMddhhmmssSSS
	 */
	public static final String YYYYMMDDHHMMSSSSS = "yyyyMMddhhmmssSSS";
	/**
	 * 日期格式：yyyyMMdd
	 */
	public static final String YYYYMMDD = "yyyyMMdd";
	/**
	 * 日期格式：【yyyy】年【MM】月【dd】
	 */
	public static final String DATE_PATTERN_DAY_CN_1 = "【yyyy】年【MM】月【dd】";
	/**
	 * 日期格式：yyyy年MM月dd日
	 */
	public static final String DATE_PATTERN_DAY_CN_2 = "yyyy年MM月dd日";
	/**
	 * 每天的最早时间点：" 00:00:00"
	 */
	public static final String DAY_FIRST_TIME = " 00:00:00";
	/**
	 * 每天的最晚时间点： " 23:59:59"
	 */
	public static final String DAY_LAST_TIME = " 23:59:59";

	public enum DateMode{
		year,month,day,week,hour,minute,seconds,millis
	}

	public static String[] weeks = { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };

	public static final int DAY_HOURS = 24; // 全天24小时

	public static List<Date> dayHourList = new ArrayList<Date>();

	static {
	    for (int i = 0; i < DAY_HOURS; i++) {
			dayHourList.add(DateUtils.parse(i + ":" + "00", DateUtils.HH_MM));
        }
	}

	/**
	 * 获取当前日期
	 * @return
	 */
	public static Date getCurrentDate() {
		return new Date();
	}

    /**
     *
     * 获取指定格式的当前日期字符串
     * @param pattern 字符串时间格式
     * @return
     * @throws ParseException
     */
	public static String getCurrentDate(String pattern) {
        return formatDate(getCurrentTime(), pattern);
    }

	/**
	 * 获取当前时间
	 * @return long 类型
	 */
	public static long getCurrentTime() {
		return System.currentTimeMillis();
	}

	/**
	 * 日期转指定格式的日期字符串，
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static String dateToString(Date date, String pattern) {
        if (StringUtils.isBlank(pattern)) {
            pattern = DATE_PATTERN_DEFAULT;
        }
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        String str = formatter.format(date);
        return str;
    }

	/**
	 *
	 * 字符串转指定日期格式的日期，
	 * @param str
	 * @param pattern
	 * @return
	 */
	public static Date stringToDate(String str, String pattern) {
		Date dateTime = null;
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		try {
			dateTime = formatter.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return dateTime;
	}

	/**
	 * 时间字符串转本地时间
	 * @param timeStr
	 * @return
	 */
	public static LocalDateTime strToLocalDateTime(String timeStr) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN_DEFAULT);
		LocalDateTime time = LocalDateTime.parse(timeStr, formatter);
		return time;
	}

	/**
	 *
	 * @Description 格式化日期
	 * @return
	 * @throws ParseException
	 */
	public static String formatDate(Date date) {
		return dateToString(date == null ? new Date() : date, DATE_PATTERN_DEFAULT);
	}

	/**
	 *
	 * @Description 格式化日期
	 * @param pattern 字符串时间格式
	 * @return
	 * @throws ParseException
	 */
	public static String formatDate(Date date, String pattern) {
        return dateToString(date == null ? new Date() : date, pattern);
    }

	/**
	 *
	 * @Description 将毫秒数转为指定格式的字符串类型时间
     * @param millisecond 毫秒数
	 * @param pattern 字符串时间格式
	 * @return
	 * @throws ParseException
	 */
	public static String formatDate(long millisecond, String pattern) {
		return dateToString(new Date(millisecond) , pattern);
	}

	/**
	 * 将毫秒数转为日期
	 * @param millisecond 毫秒数
	 * @return
	 */
	public static Date formatDate(long millisecond) {
		return new Date(millisecond);
	}

	/**
	 *
	 * @Description 将字符串时间转化为日期
	 * @param date 日期
	 * @param pattern 字符串时间格式
	 * @return
	 * @throws ParseException
	 */
	public static Date formatDate(String date, String pattern) {
		return stringToDate(date, pattern);
	}

	/**
	 * 获取指定日期N个月后的日期
	 * @param inputDate
	 * @param number
	 * @return
	 */
	public static String getAfterMonth(String inputDate, int number) {
		Calendar c = Calendar.getInstance();//获得一个日历的实例
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try{
			date = sdf.parse(inputDate);//初始日期
		}catch(Exception e){

		}
		c.setTime(date);//设置日历时间
		c.add(Calendar.MONTH,number);//在日历的月份上增加6个月
		String strDate = sdf.format(c.getTime());//的到你想要得6个月后的日期
		return strDate;
	}

	/**
	 * 字符串转日期（指定日期格式）
	 * @param str
	 * @param pattern
	 * @return
	 */
	public static Date parse(String str, String pattern) {
        Date dateTime = null;
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        try {
            dateTime = formatter.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateTime;
    }

	/**
	 * @Description 日期转星期
	 * @param date
	 * @return
	 */
    public static String dateToWeek(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int w = calendar.get(Calendar.DAY_OF_WEEK) - 1; // 指示1个星期中的某天
        if (w < 0)
            w = 0;

        return weeks[w];
    }

    /**
     * 日期+N天
     * @param date
     * @return
     */
    public static Date addDay(Date date, int days) {
        Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);
        return parse(formatDate(cal.getTime(), YYYY_MM_DD), YYYY_MM_DD);
    }

	/**
	 * 日期+N天
	 * @param date 需要修改的时间
	 * @param days 增加的天数
	 * @param pattern 字符串时间格式
	 * @return
	 */
	public static Date addDay(Date date, int days, String pattern) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);
		return parse(formatDate(cal.getTime(), pattern), pattern);
	}

	/**
	 * 日期-N天
	 * @param date
	 * @return
	 */
	public static Date minusDay(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(cal.DATE, -days);
		return parse(formatDate(cal.getTime(), YYYY_MM_DD), YYYY_MM_DD);
	}

	/**
	 * 日期+N小时
	 * @param date
	 * @param hours
	 * @return
	 */
	public static String addHour(Date date, int hours) {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN_TIME_EN);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR, hours);
		return sdf.format(cal.getTime());
	}

	/**
	 * 日期+N小时 (返回值带秒)
	 * @param date
	 * @param hours
	 * @return
	 */
	public static String addHourWithSecond(Date date, int hours) {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN_DEFAULT);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR, hours);
		return sdf.format(cal.getTime());
	}

	/**
	 * 日期-N小时
	 * @param date
	 * @param hours
	 * @return
	 */
	public static String minusHour(Date date, int hours) {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN_TIME_EN);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR, -hours);
		return sdf.format(cal.getTime());
	}

	/**
	 * 日期+N分钟
	 * @param date
	 * @param minutes
	 * @return
	 */
	public static String addMinute(Date date, int minutes) {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN_TIME_EN);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MINUTE, minutes);
		return sdf.format(cal.getTime());
	}

	/**
	 * 日期-N分钟
	 * @param date
	 * @param minutes
	 * @return
	 */
	public static String minusMinute(Date date, int minutes) {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN_TIME_EN);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MINUTE, -minutes);
		return sdf.format(cal.getTime());
	}

	/**
	 * 日期+N秒钟
	 * @param date
	 * @return
	 */
	public static Date addSecond(Date date, int seconds) {
		date.setTime(date.getTime()+seconds*1000);
		return date;
	}

	/**
	 * 日期-N秒钟
	 * @param date
	 * @return
	 */
	public static Date minusSecond(Date date, int seconds) {
		date.setTime(date.getTime()-seconds*1000);
		return date;
	}

    /**
     * @Description 验证两段日期是否有交集
     * @param a1 第一段开始时间
     * @param a2 第一段结束时间
     * @param b1 第二段开始时间
     * @param b2 第二段结束时间
     * @return
     */
    public static boolean isDateSpanCross(Date a1, Date a2, Date b1, Date b2) {
        if ((a1.getTime() <= b1.getTime()) &&
                a2.getTime() >= b1.getTime()) {
            return true;
        } else if ((a1.getTime() >= b1.getTime()) &&
                a1.getTime() <= b2.getTime()) {
            return true;
        } else {
            return false;
        }
    }

	/**
	 * 获取日期对应的年份
	 * @param date
	 * @return
	 */
    public static int getYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(cal.YEAR);
    }

	/**
	 * 获取日期对应的月份
	 * @param date
	 * @return
	 */
	public static int getMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(cal.MONTH) + 1;
    }

	/**
	 * 获取日期对应的某月某天
	 * @param date
	 * @return
	 */
	public static int getDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(cal.DATE);
    }

    /**
     * @Description: 计算时间差（分钟）
     * @param time1
     * @param time2
     * @return
     * @throws ParseException
     */
    public static long timeDifference(String time1, String time2){
    	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		Date st;
		Date st1;
		long l=0;
		try {
			st = sdf.parse(time1);
			st1 = sdf.parse(time2);
			long h=0;
			 if(st.compareTo(st1)==1){
				   h =st.getTime()-st1.getTime(); //获取时间差
			   }else if(st.compareTo(st1)==-1){
				   h=st1.getTime()-st.getTime();
			   }else if(st.compareTo(st1)==0){
				   h=st.getTime()-st1.getTime();
			   }
			l=h/1000/60;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return l;
   }

	/**
	 * 与当前时间对比，判断是否过期
	 * @param strDate
	 * @return
	 */
	public static boolean dateCompare(String strDate){
		SimpleDateFormat df = new SimpleDateFormat(DATE_PATTERN_DEFAULT);
		String nowDate=df.format(new Date());
		try {
			Date now = df.parse(nowDate);
			Date str=df.parse(strDate);
			  //大于等于当前时间，true
			  if(str.compareTo(now)>=0){
				  return true;
			   }else {
				  return false;
			   }
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	/**
	 *
	 * @param oneDate 加上前日期
	 * @param plusQuantity 加上的数量
	 * @param plusMode 加上的模式
	 * @return 相加后的结果
	 */
	public static Date datePlus(Date oneDate, long plusQuantity, DateMode plusMode) {
		if(plusMode== DateMode.month||plusMode== DateMode.year){//不确定的毫秒数
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(oneDate);
			if(plusMode== DateMode.month){//按月
				long pastYears = plusQuantity/12;//获得加上的年数
				long pastMonths = plusQuantity%12;//获得加上的月数

				calendar.set(Calendar.YEAR,calendar.get(Calendar.YEAR)+(int)pastYears);

				int month = calendar.get(Calendar.MONTH);//
				pastMonths = month + pastMonths;//结果可能大于11(11表示12月)

				pastYears = pastMonths/12;//可能为0或1;
				pastMonths = pastMonths%12;//最终的月份数

				calendar.set(Calendar.YEAR,calendar.get(Calendar.YEAR)+(int)pastYears);
				calendar.set(Calendar.MONTH,(int)pastMonths);
			}else {
				calendar.set(Calendar.YEAR,calendar.get(Calendar.YEAR)+(int)plusQuantity);
			}
			return calendar.getTime();
		} else {//确定的毫秒数
			long oneTime = oneDate.getTime();
			long time = 0;
			if(plusMode== DateMode.millis){
				time = oneTime + plusQuantity;
			}else if(plusMode== DateMode.seconds){
				time = oneTime + (plusQuantity*1000);
			}else if(plusMode== DateMode.minute){
				time = oneTime + (plusQuantity*1000*60);
			}else if(plusMode== DateMode.hour){
				time = oneTime + (plusQuantity*1000*60*60);
			}else if(plusMode== DateMode.day){
				time = oneTime + (plusQuantity*1000*60*60*24);
			}else if(plusMode== DateMode.week){
				time = oneTime + (plusQuantity*1000*60*60*24*7);
			}

			return new Date(time);
		}
	}

	/*****汉字表达的时间格式*****/
	public final static String yyTD = "yy年";
	public final static String yyMMTD = "yy年MM个月";
	public final static String yyMMddTD = "yy年MM个月dd天";
	public final static String yyMMddHHTD = "yy年MM个月dd天HH小时";
	public final static String yyMMddHHmmTD = "yy年MM个月dd天 HH小时mm分钟";
	public final static String yyMMddHHmmssTD = "yy年MM个月dd天 HH小时mm分钟ss秒";

	public final static String yyddTD = "yy年零dd天";
	public final static String MMddTD = "MM个月dd天";
	public final static String ddHHmmTD = "dd天HH小时mm分钟";
	public final static String ddHHTD = "dd天HH小时";
	public final static String HHmmTD = "HH小时mm分钟";
	public final static String mmssTD = "mm分钟ss秒";

	public final static String MMTD = "MM个月";
	public final static String ddTD = "dd天";
	public final static String HHTD = "HH小时";
	public final static String mmTD = "mm分钟";
	public final static String ssTD = "ss秒";

	public static String formatTimeDifference(Date time1, Date time2){
		return formatTimeDifference(time1,time2, yyMMddHHmmssTD);
	}

	public static String formatTimeDifference(Date time1,Date time2, String pattern){
		return formatTimeDifference(Math.abs(time1.getTime()-time2.getTime()), pattern);
	}

	/** 毫秒数所表示的时间差值
	 * @param time 毫秒数
	 * @param pattern 返回的格式
	 * @return 相差时间 ，例： 1年；425天21小时；
	 */
	public static String formatTimeDifference(long time, String pattern){
		String formatTimeStr = "";
		long yTime = (365*24*60*60*1000L),
				MTime = (30*24*60*60*1000L),
				dTime = (24*60*60*1000),
				HTime = (60*60*1000),
				mTime = (60*1000),
				sTime = (1000);

		long yy = 0L,MM= 0L,dd= 0L,HH= 0L,mm= 0L,ss= 0L;

		if(pattern.contains("yy")){
			yy = (time)/yTime;
			if(pattern.contains("MM")){
				MM = (time%yTime)/MTime;
				if(pattern.contains("dd")){
					dd = ((time%yTime)%MTime)/dTime;
					if(pattern.contains("HH")){
						HH = (((time%yTime)%MTime)%dTime)/HTime;
						if(pattern.contains("mm")){
							mm = ((((time%yTime)%MTime)%dTime)%HTime)/mTime;
							if(pattern.contains("ss")){
								ss = (((((time%yTime)%MTime)%dTime)%HTime)%mTime)/sTime;
							}
						}
					}
				}
			}
		}else if(pattern.contains("MM")){
			MM = (time)/MTime;
			if(pattern.contains("dd")){
				dd = ((time)%MTime)/dTime;
				if(pattern.contains("HH")){
					HH = (((time)%MTime)%dTime)/HTime;
					if(pattern.contains("mm")){
						mm = ((((time)%MTime)%dTime)%HTime)/mTime;
						if(pattern.contains("ss")){
							ss = (((((time)%MTime)%dTime)%HTime)%mTime)/sTime;
						}
					}
				}
			}
		}else if(pattern.contains("dd")){
			dd = (time)/dTime;
			if(pattern.contains("HH")){
				HH = ((time)%dTime)/HTime;
				if(pattern.contains("mm")){
					mm = (((time)%dTime)%HTime)/mTime;
					if(pattern.contains("ss")){
						ss = ((((time)%dTime)%HTime)%mTime)/sTime;
					}
				}
			}
		}else if(pattern.contains("HH")){
			HH = (time)/HTime;
			if(pattern.contains("mm")){
				mm = ((time)%HTime)/mTime;
				if(pattern.contains("ss")){
					ss = (((time)%HTime)%mTime)/sTime;
				}
			}
		}else if(pattern.contains("mm")){
			mm = (time)/mTime;
			if(pattern.contains("ss")){
				ss = ((time)%mTime)/sTime;
			}
		}else if(pattern.contains("ss")){
			ss = (time)/sTime;
		}
		formatTimeStr = pattern.replaceAll("yy",String.valueOf(yy));
		formatTimeStr = formatTimeStr.replaceAll("MM",String.valueOf(MM));
		formatTimeStr = formatTimeStr.replaceAll("dd",String.valueOf(dd));
		formatTimeStr = formatTimeStr.replaceAll("HH",String.format("%02d",HH));
		formatTimeStr = formatTimeStr.replaceAll("mm",String.format("%02d",mm));
		formatTimeStr = formatTimeStr.replaceAll("ss",String.format("%02d",ss));

		return formatTimeStr;
	}

	/**
	 * 获取某个日期的开始时间戳
	 * @param date
	 * @return
	 */
	public static Timestamp getDayStartTime(Date date) {
		Calendar calendar = Calendar.getInstance();
		if (null != date) {
			calendar.setTime(date);
		}
		calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
				calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return new Timestamp(calendar.getTimeInMillis());
	}

	/**
	 * 获取某个日期的结束时间戳
	 * @param date
	 * @return
	 */
	public static Timestamp getDayEndTime(Date date) {
		Calendar calendar = Calendar.getInstance();
		if (null != date) {
			calendar.setTime(date);
		}
		calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
				calendar.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return new Timestamp(calendar.getTimeInMillis());
	}

	/**
	 * 获取时间差（天）
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws ParseException
	 */
	public static int getTimeDifferenceDay(Date startDate, Date endDate) {
		int days = (int) ((endDate.getTime() - startDate.getTime()) / (1000*3600*24));
		return days;
	}

	/**
	 *  获取时间差（天）
	 * @param time1
	 * @param time2
	 * @return
	 */
	public static double getTimeDifferenceDay(String time1, String time2) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date st1;
		Date st2;
		double d = 0d;
		try {
			st1 = sdf.parse(time1);
			st2 = sdf.parse(time2);
			long h = 0L;
			if (st1.compareTo(st2) == 1) {//st1比st2晚
				h = st1.getTime() - st2.getTime();
			} else if (st1.compareTo(st2) == -1) {//st1比st2早
				h = st2.getTime() - st1.getTime();
			} else if (st1.compareTo(st2) == 0) {//st1和st2相同
				h = st1.getTime() - st2.getTime();
			}
			d = (double) h / (1000 * 3600 * 24);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		BigDecimal b = new BigDecimal(d);
		double res = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		return res;
	}

	/**
	 *  获取时间差（小时）
	 * @param time1
	 * @param time2
	 * @return
	 */
	public static double getTimeDifferenceHour(String time1, String time2) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date st1;
		Date st2;
		double d = 0d;
		try {
			st1 = sdf.parse(time1);
			st2 = sdf.parse(time2);
			long h = 0L;
			if (st1.compareTo(st2) == 1) {//st1比st2晚
				h = st1.getTime() - st2.getTime();
			} else if (st1.compareTo(st2) == -1) {//st1比st2早
				h = st2.getTime() - st1.getTime();
			} else if (st1.compareTo(st2) == 0) {//st1和st2相同
				h = st1.getTime() - st2.getTime();
			}
			d = (double) h / (1000 * 3600);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		BigDecimal b = new BigDecimal(d);
		double res = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		return res;
	}

	/**
	 *  获取时间差（分钟）
	 * @param time1
	 * @param time2
	 * @return
	 */
	public static double getTimeDifferenceMinute(String time1, String time2) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date st1;
		Date st2;
		double d = 0d;
		try {
			st1 = sdf.parse(time1);
			st2 = sdf.parse(time2);
			long h = 0L;
			if (st1.compareTo(st2) == 1) {//st1比st2晚
				h = st1.getTime() - st2.getTime();
			} else if (st1.compareTo(st2) == -1) {//st1比st2早
				h = st2.getTime() - st1.getTime();
			} else if (st1.compareTo(st2) == 0) {//st1和st2相同
				h = st1.getTime() - st2.getTime();
			}
			d = (double) h / (1000 * 60);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		BigDecimal b = new BigDecimal(d);
		double res = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		return res;
	}

	/**
	 * 获取时间差（秒）
	 * @param time1
	 * @param time2
	 * @return
	 */
	public static double getTimeDifferenceSecond(String time1, String time2) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date st1;
		Date st2;
		double d = 0d;
		long ms = 1000L;// 1秒钟的毫秒数
		try {
			st1 = sdf.parse(time1);
			st2 = sdf.parse(time2);
			long h = 0L;
			if (st1.compareTo(st2) == 1) {//st1比st2晚
				h = st1.getTime() - st2.getTime();
			} else if (st1.compareTo(st2) == -1) {//st1比st2早
				h = st2.getTime() - st1.getTime();
			} else if (st1.compareTo(st2) == 0) {//st1和st2相同
				h = 0L;
			}
			d = h / ms;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		BigDecimal b = new BigDecimal(d);
		double res = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		return res;
	}

	public static boolean isDate(String date) {
		/**
		 * 判断日期格式和范围
		 */
		String rexp = "^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))";

		Pattern pat = Pattern.compile(rexp);

		Matcher mat = pat.matcher(date);

		boolean dateType = mat.matches();

		return dateType;
	}

	/**
	 * 判断是否是时间HH:mm:ss
	 */
	public static boolean isTime(String time) {

		if(StringUtils.isEmpty(time)){
			return false;
		}

		Pattern p = Pattern.compile("(([01]\\d)|(2[0-3])):[0-5]\\d(:[0-5]\\d)");

		return p.matcher(time).matches();

	}

	/**
	 * 获取两个日期相差的月数
	 */
	public static int getMonthDiff(Date d1, Date d2) {

		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		c1.setTime(d1);
		c2.setTime(d2);
		int year1 = c1.get(Calendar.YEAR);
		int year2 = c2.get(Calendar.YEAR);
		int month1 = c1.get(Calendar.MONTH);
		int month2 = c2.get(Calendar.MONTH);
		int day1 = c1.get(Calendar.DAY_OF_MONTH);
		int day2 = c2.get(Calendar.DAY_OF_MONTH);
		// 获取年的差值
		int yearInterval = year1 - year2;
		// 如果 d1的 月-日 小于 d2的 月-日 那么 yearInterval-- 这样就得到了相差的年数
		if (month1 < month2 || month1 == month2 && day1 < day2) {
			yearInterval--;
		}
		// 获取月数差值
		int monthInterval = (month1 + 12) - month2;
		if (day1 < day2) {
			monthInterval--;
		}
		monthInterval %= 12;
		int monthsDiff = Math.abs(yearInterval * 12 + monthInterval);
		return monthsDiff;
	}

	/**
	 * 获取指定日期加num个月后日期
	 */
	public static Date addMonth(Date d1, int num) {

		if(d1 == null){
			d1 = new Date();
		}
		SimpleDateFormat sf = new SimpleDateFormat(DateUtils.YYYY_MM_DD);
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(d1);
		gc.add(Calendar.MONTH,num);
		gc.set(gc.get(Calendar.YEAR),gc.get(Calendar.MONTH),gc.get(Calendar.DATE));

		return gc.getTime();
	}

	/**
	 * 获取指定日期当前周第一天
	 */
	public static String getCurrentWeekFirstDay(Date d1) {

		if(d1 == null){
			d1 = new Date();
		}
		SimpleDateFormat sf = new SimpleDateFormat(DateUtils.YYYY_MM_DD);
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(d1);
		gc.set(Calendar.DAY_OF_WEEK,2);
		gc.set(Calendar.HOUR_OF_DAY,0);
		gc.set(Calendar.MINUTE,0);
		gc.set(Calendar.SECOND,0);
		gc.set(Calendar.MILLISECOND,0);

		return sf.format(gc.getTime());
	}

	/**
	 * 获取指定日期当前周最后一天
	 */
	public static String getCurrentWeekLastDay(Date d1) {

		if(d1 == null){
			d1 = new Date();
		}
		SimpleDateFormat sf = new SimpleDateFormat(DateUtils.YYYY_MM_DD);
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(DateUtils.formatDate(getCurrentWeekFirstDay(d1),DateUtils.YYYY_MM_DD));
		gc.add(Calendar.DAY_OF_MONTH, 6);

		return sf.format(gc.getTime());
	}


	public static void main(String[] args) {

	}

}

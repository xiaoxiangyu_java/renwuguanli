package home.study.xxy.bs.utils;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.io.*;
import java.net.*;
import java.nio.charset.Charset;
import java.util.*;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
public class HttpClientUtils {

    public static String doGet(String url, Map<String, String> param, Map<String, String> header) {
        String resultString = null;
        CloseableHttpResponse response;
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            URIBuilder builder = new URIBuilder(url);
            if (param != null) {
                for (String key : param.keySet()) {
                    builder.addParameter(key, param.get(key));
                }
            }
            URI uri = builder.build();
            HttpGet httpGet = new HttpGet(uri);
            httpGet = (HttpGet) setHeader(httpGet, header);
            response = httpClient.execute(httpGet);
            resultString = dealResponse(response);
        } catch (IOException | URISyntaxException e) {
            log.error(e.getMessage());
        }

        return resultString;
    }

    /**
     * form表单
     * @param url
     * @param param
     * @param header
     * @return
     */
    public static String doPost(String url, Map<String, Object> param, Map<String, String> header) {
        CloseableHttpResponse response;
        String resultString = null;
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost(url);
            httpPost = (HttpPost) setHeader(httpPost, header);
            if (param != null) {
                List<NameValuePair> paramList = new ArrayList<>();
                for (String key : param.keySet()) {
                    paramList.add(new BasicNameValuePair(key, (String) param.get(key)));
                }
                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(paramList);
                httpPost.setEntity(entity);
            }
            response = httpClient.execute(httpPost);
            resultString = dealResponse(response);
        } catch (IOException  e) {
            log.error(e.getMessage());

        }

        return resultString;
    }

    /**
     * HTTPS请求
     * @param requestUrl 地址
     * @param requestMethod GET、POST
     * @param bodyData 参数
     * @return
     */
    public static JSONObject httpsRequest(String requestUrl, String requestMethod, String bodyData) {
        JSONObject jsonObject = null;
        StringBuffer buffer = new StringBuffer();
        try {
            /** 创建SSLContext对象，并使用我们指定的信任管理器初始化 **/
            TrustManager[] tm = { new MyX509TrustManager() };
            SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
            sslContext.init(null, tm, new java.security.SecureRandom());
            /** 从上述SSLContext对象中得到SSLSocketFactory对象  **/
            javax.net.ssl.SSLSocketFactory ssf = sslContext.getSocketFactory();

            URL url = new URL(requestUrl);
            HttpsURLConnection httpUrlConn = (HttpsURLConnection) url.openConnection();
            httpUrlConn.setSSLSocketFactory(ssf);
            httpUrlConn.setDoOutput(true);
            httpUrlConn.setDoInput(true);
            httpUrlConn.setUseCaches(false);
            httpUrlConn.setConnectTimeout(3000);
            httpUrlConn.setReadTimeout(3000);

            /** 设置请求方式（GET/POST） **/
            httpUrlConn.setRequestMethod(requestMethod);

            if ("GET".equalsIgnoreCase(requestMethod)) {
                httpUrlConn.connect();
            }

            /** 当有数据需要提交时 **/
            if (null != bodyData) {
                OutputStream outputStream = httpUrlConn.getOutputStream();
                /** 注意编码格式，防止中文乱码  **/
                outputStream.write(bodyData.getBytes("UTF-8"));
                outputStream.close();
            }

            /** 将返回的输入流转换成字符串 **/
            InputStream inputStream = httpUrlConn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            String str = null;
            while ((str = bufferedReader.readLine()) != null) {
                buffer.append(str);
            }
            bufferedReader.close();
            inputStreamReader.close();
            /** 释放资源  **/
            inputStream.close();
            inputStream = null;
            httpUrlConn.disconnect();
            jsonObject = JSONObject.parseObject(buffer.toString());
        } catch (ConnectException ce) {
            System.out.println("Weixin server connection timed out.");
        } catch (Exception e) {
            System.out.println("https request error:{}"+e);
        }
        return jsonObject;
    }

    public static String doGet(String url, Map<String, String> param) {

        // 创建Httpclient对象
        CloseableHttpClient httpclient = HttpClients.createDefault();

        String resultString = "";
        CloseableHttpResponse response = null;
        try {
            // 创建uri
            URIBuilder builder = new URIBuilder(url);
            if (param != null) {
                for (String key : param.keySet()) {
                    builder.addParameter(key, param.get(key));
                }
            }
            URI uri = builder.build();

            // 创建http GET请求
            HttpGet httpGet = new HttpGet(uri);

            // 执行请求
            response = httpclient.execute(httpGet);
            // 判断返回状态是否为200
            if (response.getStatusLine().getStatusCode() == 200) {
                resultString = EntityUtils.toString(response.getEntity(), "UTF-8");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return resultString;
    }

    public static String doGet(String url) {
        return doGet(url, null);
    }

    public static String doPost(String url, Map<String, String> param) {
        // 创建Httpclient对象
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        String resultString = "";
        try {
            // 创建Http Post请求
            HttpPost httpPost = new HttpPost(url);
            // 创建参数列表
            if (param != null) {
                List<NameValuePair> paramList = new ArrayList<NameValuePair>();
                for (String key : param.keySet()) {
                    paramList.add(new BasicNameValuePair(key, param.get(key)));
                }
                // 模拟表单
                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(paramList);
                httpPost.setEntity(entity);
            }
            // 执行http请求
            response = httpClient.execute(httpPost);
            resultString = EntityUtils.toString(response.getEntity(), "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return resultString;
    }

    /**
     * json
     * @param url
     * @param json
     * @param header
     * @return
     */
    public static String doPostJson(String url, String json, Map<String, String> header){
        CloseableHttpResponse response;
        String resultString = null;
        try (CloseableHttpClient httpClient = HttpClients.createDefault()){
            HttpPost httpPost = new HttpPost(url);
            httpPost = (HttpPost) setHeader(httpPost, header);
            StringEntity entity = new StringEntity(json, ContentType.TEXT_PLAIN.withCharset("UTF-8"));
            entity.setContentType("application/json;charset=utf-8");
            httpPost.setEntity(entity);
            response = httpClient.execute(httpPost);
            resultString = dealResponse(response);
        } catch (IOException e) {
            log.error(e.getMessage());
            
        }

        return resultString;
    }

    /**
     * 多文件
     * @param url
     * @param params
     * @param files
     * @param headers
     * @return
     */
    public static String httpPostFormMultipartFiles(String url, Map<String,String> params, List<File> files, Map<String,String> headers){
        String encode = "utf-8";
        String resultString = null;
        CloseableHttpResponse httpResponse;
        try(CloseableHttpClient closeableHttpClient = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost(url);
            /** 设置header **/
            httpPost = (HttpPost) setHeader(httpPost, headers);
            MultipartEntityBuilder mEntityBuilder = MultipartEntityBuilder.create();
            mEntityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            mEntityBuilder.setCharset(Charset.forName(encode));
            // 普通参数
            ContentType contentType = ContentType.create("text/plain", Charset.forName(encode));//解决中文乱码
            if (params != null && params.size() > 0) {
                Set<String> keySet = params.keySet();
                for (String key : keySet) {
                    mEntityBuilder.addTextBody(key, params.get(key), contentType);
                }
            }
            /** 二进制参数 **/
            if (files != null && files.size() > 0) {  //多文件处理
                for (File file : files) {
                    mEntityBuilder.addBinaryBody("file", file);
                }
            }
            httpPost.setEntity(mEntityBuilder.build());
            httpResponse = closeableHttpClient.execute(httpPost);
            resultString = dealResponse(httpResponse);
        }catch (Exception e){
            log.error(e.getMessage());
            
        }

        return resultString;
    }

    /**
     * 单文件
     * @param url
     * @param params
     * @param file
     * @param headers
     * @return
     */
    public static String httpPostFormMultipartFile(String url, Map<String,String> params, File file, Map<String,String> headers){
        String encode = "utf-8";
        String resultString = null;
        CloseableHttpResponse httpResponse;
        try(CloseableHttpClient closeableHttpClient = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost(url);
            /** 设置header **/
            httpPost = (HttpPost) setHeader(httpPost, headers);
            MultipartEntityBuilder mEntityBuilder = MultipartEntityBuilder.create();
            mEntityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            mEntityBuilder.setCharset(Charset.forName(encode));
            /** 普通参数 **/
            ContentType contentType = ContentType.create("text/plain", Charset.forName(encode));//解决中文乱码
            if (params != null && params.size() > 0) {
                Set<String> keySet = params.keySet();
                for (String key : keySet) {
                    mEntityBuilder.addTextBody(key, params.get(key), contentType);
                }
            }
            /** 二进制参数 **/
            if (file != null) {
                mEntityBuilder.addBinaryBody("file", file);
            }
            httpPost.setEntity(mEntityBuilder.build());
            httpResponse = closeableHttpClient.execute(httpPost);
            resultString = dealResponse(httpResponse);
        }catch (Exception e){
            log.error(e.getMessage());
            
        }

        return resultString;
    }

    /**
     * zimg单文件上传
     * @param url
     * @param file
     * @return
     */
    public static String postFileToImage(String url,File file){
        String fileName = file.getName();
        String ext = fileName.substring(fileName.lastIndexOf(".") + 1);
        String resultString = null;
        Map<String,String> headers = new HashMap<>();
        headers.put("Connection", "Keep-Alive");
        headers.put("Cache-Control", "no-cache");
        headers.put("Content-Type", ext.toLowerCase());
        headers.put("COOKIE", "qixun");
        CloseableHttpResponse httpResponse;
        try(CloseableHttpClient closeableHttpClient = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost(url);
            /** 设置header **/
            httpPost = (HttpPost) setHeader(httpPost, headers);
            byte[] bytes = StringFileUtils.file2byte(file);
            ByteArrayEntity byteArrayEntity = new ByteArrayEntity(bytes);
            httpPost.setEntity(byteArrayEntity);
            httpResponse = closeableHttpClient.execute(httpPost);
            resultString = dealResponse(httpResponse);
        }catch (Exception e){
            e.printStackTrace();
      
        }

        return resultString;
    }

    /**
     * zimg多文件上传
     * @param url
     * @param file
     * @return
     */
    public static String postMultipartFileToImage(String url, MultipartFile file){
        String fileName = file.getOriginalFilename();
        String ext = fileName.substring(fileName.lastIndexOf(".") + 1);
        String resultString = null;
        Map<String,String> headers = new HashMap<>();
        headers.put("Connection", "Keep-Alive");
        headers.put("Cache-Control", "no-cache");
        headers.put("Content-Type", ext.toLowerCase());
        headers.put("COOKIE", "qixun");
        CloseableHttpResponse httpResponse;
        try(CloseableHttpClient closeableHttpClient = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost(url);
            //设置header
            httpPost = (HttpPost) setHeader(httpPost, headers);
            byte[] bytes = StringFileUtils.toByteArray(file.getInputStream());
            ByteArrayEntity byteArrayEntity = new ByteArrayEntity(bytes);
            httpPost.setEntity(byteArrayEntity);
            httpResponse = closeableHttpClient.execute(httpPost);
            resultString = dealResponse(httpResponse);
        }catch (Exception e){
            e.printStackTrace();
            
        }

        return resultString;
    }

    private static HttpRequestBase setHeader(HttpRequestBase httpRequestBase, Map<String, String> header) {
        if (header != null) {
            for (Map.Entry<String, String> entry : header.entrySet()) {
                httpRequestBase.setHeader(entry.getKey(), entry.getValue());
            }
        }
        return httpRequestBase;
    }

    private static String dealResponse(CloseableHttpResponse response) {
        String resultString = null;
        String successStatus = "20";
        try {
            if (response!=null&&String.valueOf(response.getStatusLine().getStatusCode()).contains(successStatus)) {
                resultString = EntityUtils.toString(response.getEntity(), "UTF-8");
                log.info(resultString);
            }
        } catch (IOException e) {
            log.error(e.getMessage());

        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    log.error(e.getMessage());

                }
            }
        }
        return resultString;
    }

    public static String sendGet(String url, String param, String contentType)
    {
        StringBuilder result = new StringBuilder();
        BufferedReader in = null;
        try
        {
            String urlNameString = url + "?" + param;
            log.info("sendGet - {}", urlNameString);
            URL realUrl = new URL(urlNameString);
            URLConnection connection = realUrl.openConnection();
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            connection.connect();
            in = new BufferedReader(new InputStreamReader(connection.getInputStream(), contentType));
            String line;
            while ((line = in.readLine()) != null)
            {
                result.append(line);
            }
            log.info("recv - {}", result);
        }
        catch (ConnectException e)
        {
            log.error("调用HttpUtils.sendGet ConnectException, url=" + url + ",param=" + param, e);
        }
        catch (SocketTimeoutException e)
        {
            log.error("调用HttpUtils.sendGet SocketTimeoutException, url=" + url + ",param=" + param, e);
        }
        catch (IOException e)
        {
            log.error("调用HttpUtils.sendGet IOException, url=" + url + ",param=" + param, e);
        }
        catch (Exception e)
        {
            log.error("调用HttpsUtil.sendGet Exception, url=" + url + ",param=" + param, e);
        }
        finally
        {
            try
            {
                if (in != null)
                {
                    in.close();
                }
            }
            catch (Exception ex)
            {
                log.error("调用in.close Exception, url=" + url + ",param=" + param, ex);
            }
        }
        return result.toString();
    }

    public static void main(String[] args) {
        File file = new File("D:\\pic4.jpg");
        String s = postFileToImage("http://39.100.94.39:4869/upload",file);
        /*ZimgResult zimgResult = JSONObject.parseObject(s,ZimgResult.class);*/
        System.out.println(s);
        System.out.println("99006d850f8daf696eede6d5070d0934".length());
    }

}


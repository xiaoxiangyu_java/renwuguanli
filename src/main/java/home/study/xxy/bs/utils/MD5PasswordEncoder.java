package home.study.xxy.bs.utils;

import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author xiaoxiangyu
 * @create 2022-05-19 22:16
 * @功能：
 */
public class MD5PasswordEncoder implements PasswordEncoder {

    @Override
    public String encode(CharSequence charSequence) {
        return MD5Utils.stringToMD5((String)charSequence);
    }

    @Override
    public boolean matches(CharSequence charSequence, String encoded) {
        return encoded.equals(MD5Utils.stringToMD5((String)charSequence));
    }
}

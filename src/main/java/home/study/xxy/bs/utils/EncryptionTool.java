package home.study.xxy.bs.utils;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.UUID;

/**
 * @Classname EncryptionTool
 * @Description 加密解密工具
 * @Date 2020/9/15 9:11
 * @Created by shir@cosber.com
 */
public class EncryptionTool {
    private static final EncryptionTool instance = new EncryptionTool();
    public static final String SHA_1_PRNG = "SHA1PRNG";
    public static final String SUN = "SUN";
    public static final int BYTES_LEN = 128;

    private EncryptionTool() {
        super();
    }

    public static EncryptionTool getInstance() {
        return instance;
    }

    /**
     * 加密解密方式
     */
    private static final String AES = "AES";

    /**
     * Method Description: 生成UUID
     *
     * @Param: []
     * @Return: java.lang.String UUID
     * @Author: shir@cosber.com
     * @Date: 2020/9/15 9:55
     */
    public String createUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 加密
     *
     * @param content  需要加密的内容
     * @param password 加密密码
     * @return
     * @throws
     * @method encrypt
     * @since v1.0
     */
    public byte[] encrypt(String content, String password) {
        byte[] result = new byte[0];
        try {
            KeyGenerator keyGen = getKeyGenerator(password);
            SecretKey secretKey = keyGen.generateKey();
            byte[] enCodeFormat = secretKey.getEncoded();
            SecretKeySpec key = new SecretKeySpec(enCodeFormat, AES);
            Cipher cipher = Cipher.getInstance(AES);// 创建密码器
            byte[] byteContent = content.getBytes(StandardCharsets.UTF_8);
            cipher.init(Cipher.ENCRYPT_MODE, key);// 初始化
            result = cipher.doFinal(byteContent);
        } catch (NoSuchAlgorithmException |
                NoSuchPaddingException |
                InvalidKeyException |
                IllegalBlockSizeException |
                BadPaddingException |
                NoSuchProviderException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 解密
     *
     * @param content  待解密内容
     * @param password 解密密钥
     * @return
     * @throws
     * @method decrypt
     * @since v1.0
     */
    public byte[] decrypt(byte[] content, String password) {
        byte[] result = new byte[0];
        try {
            KeyGenerator keyGen = getKeyGenerator(password);
            SecretKey secretKey = keyGen.generateKey();
            byte[] enCodeFormat = secretKey.getEncoded();
            SecretKeySpec key = new SecretKeySpec(enCodeFormat, AES);
            Cipher cipher = Cipher.getInstance(AES);// 创建密码器
            cipher.init(Cipher.DECRYPT_MODE, key);// 初始化
            result = cipher.doFinal(content);
        } catch (NoSuchAlgorithmException |
                NoSuchPaddingException |
                InvalidKeyException |
                IllegalBlockSizeException |
                BadPaddingException |
                NoSuchProviderException e) {
            e.printStackTrace();
        }

        return result;
    }

    private KeyGenerator getKeyGenerator(String password) throws NoSuchAlgorithmException, NoSuchProviderException {
        KeyGenerator keyGen = KeyGenerator.getInstance(AES);
        SecureRandom random = SecureRandom.getInstance(SHA_1_PRNG, SUN);
        random.setSeed(password.getBytes());
        keyGen.init(BYTES_LEN, random);
        return keyGen;
    }

    /**
     * 将二进制转换成16进制
     *
     * @param buf
     * @return
     * @throws
     * @method parseByte2HexStr
     * @since v1.0
     */
    public String parseByte2HexStr(byte buf[]) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < buf.length; i++) {
            String hex = Integer.toHexString(buf[i] & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            sb.append(hex.toUpperCase());
        }
        return sb.toString();
    }

    /**
     * 将16进制转换为二进制
     *
     * @param hexStr
     * @return
     * @throws
     * @method parseHexStr2Byte
     * @since v1.0
     */
    public byte[] parseHexStr2Byte(String hexStr) {
        if (hexStr.length() < 1)
            return null;
        byte[] result = new byte[hexStr.length() / 2];
        for (int i = 0; i < hexStr.length() / 2; i++) {
            int high = Integer.parseInt(hexStr.substring(i * 2, i * 2 + 1), 16);
            int low = Integer.parseInt(hexStr.substring(i * 2 + 1, i * 2 + 2), 16);
            result[i] = (byte) (high * 16 + low);
        }
        return result;
    }
}
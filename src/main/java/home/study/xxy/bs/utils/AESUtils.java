package home.study.xxy.bs.utils;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Arrays;
import java.util.Base64;
import java.util.Objects;

import home.study.xxy.bs.constans.Constants;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Classname AESUtils
 * @Description TODO
 * @Date 2021/3/20 22:20
 * @Created by huangh@cosber.com
 */
public class AESUtils {
	static final Logger log = LoggerFactory.getLogger(AESUtils.class);
	
	private static final String charSet = "UTF-8";

	/**
	 * 使用PKCS7Padding填充必须添加一个支持PKCS7Padding的Provider
	 * 类加载的时候就判断是否已经有支持256位的Provider,如果没有则添加进去
	 */
	static {
		if (Security.getProvider(new BouncyCastleProvider().PROVIDER_NAME) == null) {
			Security.addProvider(new BouncyCastleProvider());
		}
	}

	/**
	 * 密钥生成
	 * @param secretKey
	 * @return
	 * @throws Exception
	 */
	private static SecretKey generateKey(String secretKey) throws Exception {
		Provider p = Security.getProvider("SUN");
		SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG", p);
		secureRandom.setSeed(secretKey.getBytes());
		KeyGenerator kg = KeyGenerator.getInstance("AES");
		kg.init(256, secureRandom);
		return kg.generateKey();
	}

    /**
	 * AES加密（传入字符串）
     * @param data
     * @param keys
     * @param iv
     * @return
     */
	public static String AES_CBC_Encrypt(String data, String keys, String iv) {
		try {
			byte[] content = data.getBytes(charSet);
			SecretKeySpec key = new SecretKeySpec(keys.getBytes(charSet), "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding"); // 算法/模式/补码方式
			cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv.getBytes()));
			byte[] result = cipher.doFinal(content);

			String str = Base64.getEncoder().encodeToString(result);

			return str;
		} catch (Exception e) {
			log.error("AES_CBC_Encrypt:" + e.toString());
		}

		return null;
	}

	/**
	 * AES加密（传入字节数组）
	 * @param data
	 * @param keys
	 * @param iv
	 * @return
	 */
	public static String AES_CBC_Encrypt(String data, byte[] keys, String iv) {
		try {
			byte[] content = data.getBytes(charSet);
			SecretKeySpec key = new SecretKeySpec(keys, "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding"); // 算法/模式/补码方式
			cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv.getBytes()));
			byte[] result = cipher.doFinal(content);

			String str = Base64.getEncoder().encodeToString(result);

			return str;
		} catch (Exception e) {
			log.error("AES_CBC_Encrypt:" + e.toString());
		}

		return null;
	}

	/**
	 * AES解密（传入字符串）
	 * @param data
	 * @param keys
	 * @param iv
	 * @return
	 */
	public static String AES_CBC_Decrypt(String data, String keys, String iv) {
		log.info("data: " + data + "- keys: " + keys + "- iv: " +iv);
		try {
			byte[] content = Base64.getDecoder().decode(data);
			SecretKeySpec key = new SecretKeySpec(keys.getBytes(charSet), "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
			cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv.getBytes()));
			byte[] result = cipher.doFinal(content);

			String str = new String(result, charSet);

			return str;
		} catch (Exception e) {
			log.error("AES_CBC_Decrypt:" + e.toString());
		}

		return null;
	}

	/**
	 * AES解密（传入字节数组）
	 * @param data
	 * @param keys
	 * @param iv
	 * @return
	 */
	public static String AES_CBC_Decrypt(String data, byte[] keys, String iv) {
		try {
			byte[] content = Base64.getDecoder().decode(data);
			SecretKeySpec key = new SecretKeySpec(keys, "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
			cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv.getBytes()));
			byte[] result = cipher.doFinal(content);

			String str = new String(result, charSet);

			return str;
		} catch (Exception e) {
			log.error("AES_CBC_Decrypt:" + e.toString());
		}

		return null;
	}

	/**
	 * AES解密（传入字节数组）
	 * @param data
	 * @param keys
	 * @param iv
	 * @return
	 */
	public static String AES_CBC_Decrypt(String data, byte[] keys, byte[] iv) {
		try {
			byte[] content = Base64.getDecoder().decode(data);
			SecretKeySpec key = new SecretKeySpec(keys, "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
			cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
			byte[] result = cipher.doFinal(content);

			String str = new String(result, charSet);

			return str;
		} catch (Exception e) {
			log.error("AES_CBC_Decrypt:" + e.toString());
		}

		return null;
	}

	/**
	 * 重写解密接口
	 * @param password 加密的密码
	 * @param uuid 账户uuid
	 * @return 解密后的密码
	 */
	public static String decryptPwd(String password, String uuid) {
		byte[] pwdInDB = EncryptionTool.getInstance().parseHexStr2Byte(password);
		String decryptResult = new String(
				Objects.requireNonNull(
						EncryptionTool.getInstance().decrypt(pwdInDB, uuid)
				),
				StandardCharsets.UTF_8);
		if (!decryptResult.isEmpty()) {
			return decryptResult;
		} else {
			return null;
		}
	}

	/**
	 * 测试主函数
	 * @param arg
	 */
	public static void main(String [] arg ){
		String content = "Cosber123#";
		//content = "admin";
		String aesKey = Constants.AES_KEY + Constants.AES_KEY; //AES-256加密
		byte[] aesKeyBytes = new byte[32];

		try {
			SecretKey secretKey = generateKey(aesKey);
			byte[] genAesKeyBytes = secretKey.getEncoded();
			System.out.println(Arrays.toString(genAesKeyBytes));
			String encodedAesKey = Base64.getEncoder().encodeToString(genAesKeyBytes);//parseByte2Base64(aesKeyBytes);
			System.out.println(encodedAesKey);
			byte[] decodedAesKeyBytes = Base64.getDecoder().decode(encodedAesKey);//parseBase2Byte(encodedAesKey);
			System.out.println(Arrays.toString(decodedAesKeyBytes));

			System.arraycopy(decodedAesKeyBytes, 0, aesKeyBytes, 0, decodedAesKeyBytes.length);

		}catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("-----encrypt & decrypt with plain key-storage-----");
		String encryptStr = AES_CBC_Encrypt(content, aesKey, Constants.AES_IV);
		System.out.println("加密后-"+encryptStr);
		String decryptStr = AES_CBC_Decrypt(encryptStr, aesKey, Constants.AES_IV);
		System.out.println("解密后-"+decryptStr);

		System.out.println("-----encrypt & decrypt with base64 key-storage----");
		encryptStr = AES_CBC_Encrypt(content, aesKeyBytes, Constants.AES_IV);
		System.out.println(encryptStr);
		decryptStr = AES_CBC_Decrypt(encryptStr, aesKeyBytes, Constants.AES_IV);
		System.out.println(decryptStr);

		System.out.println("----某加密后的内容,解密后----");
		String decryptStr1 = AES_CBC_Decrypt("jBPK+5ep/P/L6W1QrkXFjw==", aesKey, Constants.AES_IV);
		System.out.println(aesKey);
		System.out.println(Constants.AES_IV);
		System.out.println("解密后:"+ decryptStr1);
	}
}

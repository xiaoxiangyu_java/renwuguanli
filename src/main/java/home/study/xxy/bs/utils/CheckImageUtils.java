package home.study.xxy.bs.utils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;


/**
 *    @author    xiaoxiangyu
 *    @Date 2021-02-21 16:08:06    
 *
 */
public class CheckImageUtils {

	StringBuffer sb;
	public StringBuffer getSb() {
		return sb;
	}
	public void setSb(StringBuffer sb) {
		this.sb = sb;
	}


	private static final long serialVersionUID = 1L;
	private static char[] ch = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
	public CheckImageUtils() {
		super();
		dre();
	}
	private static Color interLine(int Low, int High) {
		if (Low > 255) {
			Low = 255;
		}
		if (Low < 0) {
			Low = 0;
		}
		if (High > 255) {
			High = 255;
		}
		if (High < 0) {
			High = 0;
		}

		int interval = High - Low;
		int r = (int) (Low + Math.random() * interval);
		int g = (int) (Low + Math.random() * interval);
		int b = (int) (Low + Math.random() * interval);

		return new Color(r, g, b);
	}

	
	public BufferedImage dre() {
		BufferedImage bfi = new BufferedImage(160, 50, BufferedImage.TYPE_INT_RGB);
		Graphics g = bfi.getGraphics();
		g.fillRect(0, 0, 160, 50);

		Random r = new Random();
		int index;
		sb = new StringBuffer();
		for (int i = 0; i < 4; i++) {
			index = r.nextInt(ch.length);
//			g.setColor(new Color(r.nextInt(255),r.nextInt(255),r.nextInt(255)));
			g.setColor(new Color(255, 0, 0));

			Font font = new Font("宋体", 30, 40);
			g.setFont(font);
			g.drawString(ch[index] + "", (i * 40), 40);
			sb.append(ch[index]);
		}
		// 噪点
		int area = (int) (0.02 * 160 * 50);
		for (int i = 0; i < area; i++) {
			int x = (int) (Math.random() * 160);
			int y = (int) (Math.random() * 50);
			bfi.setRGB(x, y, (int) (Math.random() * 255));
		}

		// 干扰线
		for (int i = 0; i < 6; i++) {
			int xs = (int) (Math.random() * 160);
			int ys = (int) (Math.random() * 50);
			int xe = (int) (Math.random() * 160);
			int ye = (int) (Math.random() * 50);

			g.setColor(interLine(1, 255));
			g.drawLine(xs, ys, xe, ye);
		}
		
			return bfi;
		
		
}
}
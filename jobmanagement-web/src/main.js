// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ViewUI from 'view-design';
import 'view-design/dist/styles/iview.css';
import axios from 'axios'; /* 引入axios进行地址访问*/
// 引入echarts
import echarts from 'echarts'
import { setCookie, getCookie, checkCookie, clearCookie } from '@/utils/cookie';

Vue.prototype.$echarts = echarts

Vue.prototype.$setCookie = setCookie;
Vue.prototype.$getCookie = getCookie;
Vue.prototype.$checkCookie = checkCookie;
Vue.prototype.$clearCookie = clearCookie;

Vue.use(ViewUI);

Vue.prototype.$http = axios;
Vue.config.productionTip = false
// axios.defaults.withCredentials = true

router.beforeEach((to, from, next) => {
  /* 路由发生变化修改页面title */
  if (to.meta.title) {
    document.title = to.meta.title
  }
  if(to.path=='/login' || getCookie("userId")){//拦截未登录的访问
    next();
  }else{
    alert('登录信息过期,请重新登录');
    next('/login');
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

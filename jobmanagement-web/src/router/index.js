import Vue from 'vue'
import Router from 'vue-router'
import login from '@/components/login'
import indexNav from '@/components/indexNav'
import index from '@/components/index'
import notice from '@/components/notice'
import myJob from "@/components/myJob";
import addJob from "@/components/addJob";
import myOrder from "@/components/myOrder";
import myBill from "@/components/myBill";
import news from "@/components/news";
import changePassword from "@/components/changePassword";
import vip from "@/components/vip";
import jobExamine from "@/components/jobExamine";
import userManagement from "@/components/userManagement";
import jobHome from "@/components/jobHome";
import myGetJob from "@/components/myGetJob";
import bQuestionManagement from "@/components/bQuestionManagement";

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'login',
      component: login,
      meta: {
        title: '登录注册'
      }
    },
    {
      path: '/',
      name: 'indexNav',
      component: indexNav,
      children:[
        {
          path: '/',
          name: 'index',
          component: index,
          meta: {
            title: '首页'
          }
        },
        {
          path: '/notice',
          name: 'notice',
          component: notice,
          meta: {
            title: '公告'
          }
        },
        {
          path: '/myJob',
          name: 'myJob',
          component: myJob,
          meta: {
            title: '我的任务'
          }
        },
        {
          path: '/addJob',
          name: 'addJob',
          component: addJob,
          meta: {
            title: '添加任务'
          }
        },
        {
          path: '/myOrder',
          name: 'myOrder',
          component: myOrder,
          meta: {
            title: '我的订单'
          }
        },
        {
          path: '/myBill',
          name: 'myBill',
          component: myBill,
          meta: {
            title: '我的账单'
          }
        },
        {
          path: '/question',
          name: 'question',
          component: question,
          meta: {
            title: '常见问题'
          }
        },
        {
          path: '/changePassword',
          name: 'changePassword',
          component: changePassword,
          meta: {
            title: '修改密码'
          }
        },
        {
          path: '/vip',
          name: 'vip',
          component: vip,
          meta: {
            title: 'vip开通'
          }
        },
        {
          path: '/myNews',
          name: 'myNews',
          component: myNews,
          meta: {
            title: '我的消息'
          },
          children:[
            {
              path: '/news',
              name: 'news',
              component: news,
            }
          ]
        },
        {
          path: '/jobExamine',
          name: 'jobExamine',
          component: jobExamine,
          meta: {
            title: '任务审核'
          }
        },
        {
          path: '/jobDistribution',
          name: 'jobDistribution',
          component: jobDistribution,
          meta: {
            title: '任务分配'
          }
        },
        {
          path: '/userManagement',
          name: 'userManagement',
          component: userManagement,
          meta: {
            title: '用户管理'
          },
        },
        {
          path: '/jobHome',
          name: 'jobHome',
          component: jobHome,
          meta: {
            title: '任务大厅'
          }
        },
        {
          path: '/myGetJob',
          name: 'myGetJob',
          component: myGetJob,
          meta: {
            title: '我的任务'
          }
        },
        {
          path: '/bQuestionManagement',
          name: 'bQuestionManagement',
          component: bQuestionManagement,
          meta: {
            title: '公告任务管理'
          }
        }
        // bbManagement bQuestionManagement
        // jobHome myGetJob myWages
      ]
    },
  ],

})
